/*
 * Copyright (C) 2018 European Spallation Source ERIC
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package xal.app.trajectorycorrection;

import eu.ess.xaos.ui.plot.LineChartFX;
import eu.ess.xaos.ui.plot.NumberAxis;
import eu.ess.xaos.ui.plot.PluggableChartContainer;
import eu.ess.xaos.ui.plot.plugins.Pluggable;
import eu.ess.xaos.ui.plot.plugins.Plugins;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.chart.ValueAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.layout.AnchorPane;
import xal.ca.ConnectionException;
import xal.ca.GetException;
import xal.model.ModelException;
import xal.smf.Accelerator;
import xal.smf.AcceleratorNode;
import xal.smf.AcceleratorSeq;
import xal.smf.AcceleratorSeqCombo;
import xal.smf.impl.BPM;
import xal.smf.impl.DipoleCorr;

/**
 * FXML Controller class
 *
 * @author nataliamilas
 */
public class VerifyResponseController {

    private final BooleanProperty loggedIn = new SimpleBooleanProperty();
    private final List<BPM> BPMList = new ArrayList<>();
    private CorrectionMatrix Correction1to1;
    private CorrectionSVD CorrectionSVD;
    private String dataType = null;
    
    private final PluggableChartContainer chartContainer = new PluggableChartContainer();
    private LineChartFX<Number, Number> chartXY;    
    private final XYChart.Series<Number, Number> response = new XYChart.Series<>("response", FXCollections.observableArrayList());


    @FXML
    private Button buttonClose;
    @FXML
    private ComboBox<String> comboBoxCorrector;
    @FXML
    private ComboBox<String> comboBoxPlane;
    @FXML
    private AnchorPane panePlot;

    public CorrectionMatrix getCorrection1to1() {
        return Correction1to1;
    }

    public void setCorrection1to1(CorrectionMatrix Correction1to1) {
        setDataType("1to1");
        this.Correction1to1 = Correction1to1;
        //set planes
        comboBoxPlane.getItems().add("Horizontal");
        comboBoxPlane.getItems().add("Vertical");
        comboBoxPlane.setValue("Horizontal");

        //set bpm
        if (comboBoxPlane.getValue().equals("Horizontal")) {
            Correction1to1.HC.keySet().forEach(bpm -> BPMList.add(bpm));
            BPMList.sort((bpm1, bpm2) -> Double.compare(bpm1.getSDisplay(), bpm2.getSDisplay()));
            BPMList.forEach(bpm -> comboBoxCorrector.getItems().add(bpm.toString()));
        } else if (comboBoxPlane.getValue().equals("Vertical")) {
            Correction1to1.VC.keySet().forEach(bpm -> BPMList.add(bpm));
            BPMList.sort((bpm1, bpm2) -> Double.compare(bpm1.getSDisplay(), bpm2.getSDisplay()));
            BPMList.forEach(bpm -> comboBoxCorrector.getItems().add(bpm.toString()));
        }
        comboBoxCorrector.setValue(comboBoxCorrector.getItems().get(0));

        updatePlot1to1();        
        
        ValueAxis<Number> xAxisXY = new eu.ess.xaos.ui.plot.NumberAxis();
        ValueAxis<Number> yAxisXY = new eu.ess.xaos.ui.plot.NumberAxis();        

        xAxisXY.setLabel("Steerer strength (T.m)");
        yAxisXY.setLabel( "BPM reading (mm)");
        
        xAxisXY.setAnimated(false);
        yAxisXY.setAnimated(false);

        chartXY = new LineChartFX<>(xAxisXY, yAxisXY);
        
        chartXY.setAnimated(false);
        chartXY.setOnMouseClicked(event -> chartXY.requestFocus());
        chartXY.addChartPlugins(FXCollections.observableList(List.of(Plugins.all())));
        
        panePlot.getChildren().add(chartContainer);  
        chartContainer.setPluggable((Pluggable) chartXY);        
        
        ObservableList<XYChart.Series<Number, Number>> chartDataXY = FXCollections.observableArrayList();

        chartDataXY.add(response);        
       
        chartXY.setData(chartDataXY);
        chartXY.setCreateSymbols(true);        
    }

    public CorrectionSVD getCorrectionSVD() {
        return CorrectionSVD;
    }
    
    public int getMin(double a, double b, double c){
        double val = Math.min(a, Math.min(b,c));
        int result = 0;
        if(val ==a){
            result = 1;            
        } else if (val == b){
            result = 2;
        } else if (val == c){
            result = 3;
        }
        return result;
    };
    
     public int getMax(double a, double b, double c){
        double val = Math.max(a, Math.max(b,c));
        int result = 0;
        if(val ==a){
            result = 1;            
        } else if (val == b){
            result = 2;
        } else if (val == c){
            result = 3;
        }
        return result;
    };

    public void setCorrectionSVD(CorrectionSVD CorrectionSVD) {
        setDataType("SVD");        

        comboBoxPlane.setDisable(true);
        comboBoxCorrector.setDisable(true);
                
        //set planes
        //comboBoxPlane.getItems().add("Horizontal");
        //comboBoxPlane.getItems().add("Vertical");
        //comboBoxPlane.setValue("Horizontal");

        //set correctors
        //if (comboBoxPlane.getValue().equals("Horizontal")) {
        //    CorrectionSVD.HC.forEach(hc -> comboBoxCorrector.getItems().add(hc.toString()));
        //} else if (comboBoxPlane.getValue().equals("Vertical")) {
        //    CorrectionSVD.VC.forEach(vc -> comboBoxCorrector.getItems().add(vc.toString()));
        //}
        //comboBoxCorrector.setValue(comboBoxCorrector.getItems().get(0));
        

        double HC_val = 0.0;
        double VC_val = 0.0;        
        int row = 0;
        int col = 0;
        HashMap<BPM, Double> iniPosX = new HashMap();
        HashMap<BPM, Double> iniPosY = new HashMap();
        HashMap<BPM, Double> finalPosX = new HashMap();
        HashMap<BPM, Double> finalPosY = new HashMap();
        List<AcceleratorSeq> newCombo = new ArrayList<>();
        RunSimulationService simulService;
        Accelerator accl;
        //AcceleratorSeq iniSeq;
        //AcceleratorSeq finalSeq;
        
        
        List<Double> bpmPos = new ArrayList<>();
        List<Double> HCPos = new ArrayList<>();
        List<Double> VCPos = new ArrayList<>();
        CorrectionSVD.bpm.forEach((item) -> {bpmPos.add(item.getSDisplay());});
        CorrectionSVD.HC.forEach((item) -> {HCPos.add(item.getSDisplay());});
        CorrectionSVD.VC.forEach((item) -> {VCPos.add(item.getSDisplay());});
        
        accl = MainFunctions.mainDocument.getAccelerator();//CorrectionSVD.bpm.get(0).getAccelerator();
        //iniSeq = CorrectionSVD.bpm.get(0).getPrimaryAncestor();
        //finalSeq = CorrectionSVD.bpm.get(CorrectionSVD.bpm.size() - 1).getPrimaryAncestor();
        //setup simulation parameters 
        //switch(getMin(Collections.min(bpmPos), Collections.min(HCPos),Collections.min(VCPos))){
        //    case (1):
        //        iniSeq = CorrectionSVD.bpm.get(0).getPrimaryAncestor();
        //        break;
        //    case(2):
        //        iniSeq = CorrectionSVD.HC.get(0).getPrimaryAncestor();
        //        break; 
        //    case(3):
        //        iniSeq = CorrectionSVD.VC.get(0).getPrimaryAncestor();
        //        break; 
        //};
        
        //switch(getMax(Collections.min(bpmPos), Collections.min(HCPos),Collections.min(VCPos))){
        //    case (1):
        //        finalSeq = CorrectionSVD.bpm.get(CorrectionSVD.bpm.size() - 1).getPrimaryAncestor();
        //        break;
        //    case(2):
        //        finalSeq = CorrectionSVD.HC.get(CorrectionSVD.HC.size() - 1).getPrimaryAncestor();
        //        break; 
        //    case(3):
        //        finalSeq = CorrectionSVD.VC.get(CorrectionSVD.VC.size() - 1).getPrimaryAncestor();
        //        break; 
        //};           

        //if (iniSeq != finalSeq) {            
        //    for (int i = accl.getAllSeqs().indexOf(iniSeq); i <= accl.getAllSeqs().indexOf(finalSeq); i++) {
        //        newCombo.add(accl.getAllSeqs().get(i));
        //    }
        //    AcceleratorSeqCombo Sequence = new AcceleratorSeqCombo("calcMatrix", newCombo);
            //simulService = new RunSimulationService(Sequence);
        //} else {
        //    AcceleratorSeq Sequence = CorrectionSVD.bpm.get(0).getPrimaryAncestor();
            //simulService = new RunSimulationService(Sequence);
        //}     
        
        accl.getSequences().forEach(seq-> {
            if(seq.getAllNodesOfType("BPM").size()>0){
                newCombo.add(seq);
                System.out.print(seq.getId());
            }
                
        });
        
        AcceleratorSeqCombo Sequence = new AcceleratorSeqCombo("calcMatrix", newCombo);
        
        simulService = new RunSimulationService(Sequence);
        simulService.setSynchronizationMode("DESIGN");
                
        try {
            //get initial trajectory for the whole machine
            MainFunctions.mainDocument.DisplayTraj.readTrajectory(accl.getAllNodesOfType("BPM"));
            iniPosX = simulService.runTrajectorySimulation(accl.getAllNodesOfType("BPM"), "X");
            iniPosY = simulService.runTrajectorySimulation(accl.getAllNodesOfType("BPM"), "Y");
            //MainFunctions.mainDocument.DisplayTraj.readTrajectory(CorrectionSVD.bpm);
            //iniPosX = simulService.runTrajectorySimulation(CorrectionSVD.bpm, "X");
            //iniPosY = simulService.runTrajectorySimulation(CorrectionSVD.bpm, "Y");
        } catch (InstantiationException | ModelException ex) {
            Logger.getLogger(CorrectionSVD.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ConnectionException ex) {  
            Logger.getLogger(VerifyResponseController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (GetException ex) {
            Logger.getLogger(VerifyResponseController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        double[] kickH = CorrectionSVD.calculateHCorrection(MainFunctions.mainDocument.DisplayTraj);
        double[] kickV = CorrectionSVD.calculateVCorrection(MainFunctions.mainDocument.DisplayTraj);
        
        simulService.setHSteerers(CorrectionSVD.HC,kickH);
        simulService.setHSteerers(CorrectionSVD.VC,kickV);
                     
        try {
            finalPosX = simulService.runTrajectorySimulation(accl.getAllNodesOfType("BPM"), "X");
            finalPosY = simulService.runTrajectorySimulation(accl.getAllNodesOfType("BPM"), "Y");
            //finalPosX = simulService.runTrajectorySimulation(CorrectionSVD.bpm, "X");
            //finalPosY = simulService.runTrajectorySimulation(CorrectionSVD.bpm, "Y");
        } catch (InstantiationException | ModelException ex) {
            Logger.getLogger(CorrectionSVD.class.getName()).log(Level.SEVERE, null, ex);
        }            
             
        simulService.resetHSteerers(CorrectionSVD.HC,kickH);
        simulService.resetHSteerers(CorrectionSVD.VC,kickV);
        //updatePlotSVD();  
        
        //Collect the data
        XYChart.Series<Number, Number> trajectory = new XYChart.Series<>("trajectory", FXCollections.observableArrayList());
        
        ValueAxis<Number> xAxisXY = new eu.ess.xaos.ui.plot.NumberAxis();
        ValueAxis<Number> yAxisXY = new eu.ess.xaos.ui.plot.NumberAxis();   
        
        xAxisXY.setLowerBound(Math.floor(Math.min(Collections.min(bpmPos),Math.min(Collections.min(HCPos),Collections.min(VCPos)))));
        xAxisXY.setUpperBound(Math.ceil(Math.max(Collections.max(bpmPos),Math.max(Collections.max(HCPos),Collections.max(VCPos)))));

        xAxisXY.setLabel("Position (m)");
        yAxisXY.setLabel("Trajectory");
        
        xAxisXY.setAnimated(false);
        yAxisXY.setAnimated(false);

        chartXY = new LineChartFX<>(xAxisXY, yAxisXY);
        
        chartXY.setAnimated(false);
        chartXY.setOnMouseClicked(event -> chartXY.requestFocus());
        chartXY.addChartPlugins(FXCollections.observableList(List.of(Plugins.all())));
        //set position xmax and xmin in the plots 
        chartXY.getXAxis().setAutoRanging(false);               
                             
        panePlot.getChildren().add(chartContainer);  
        chartContainer.setPluggable((Pluggable) chartXY);        
        
        ObservableList<XYChart.Series<Number, Number>> chartDataXY = FXCollections.observableArrayList();
        ObservableList<XYChart.Data<Number, Number>> refXData = FXCollections.observableArrayList();
        ObservableList<XYChart.Data<Number, Number>> refYData = FXCollections.observableArrayList();
        ObservableList<XYChart.Data<Number, Number>> correctedXData = FXCollections.observableArrayList();
        ObservableList<XYChart.Data<Number, Number>> correctedYData = FXCollections.observableArrayList();
        XYChart.Series<Number, Number> correctedTrajX = new XYChart.Series<>("X corrected", FXCollections.observableArrayList());
        XYChart.Series<Number, Number> correctedTrajY = new XYChart.Series<>("Y corrected", FXCollections.observableArrayList());
        XYChart.Series<Number, Number> currentTrajX = new XYChart.Series<>("X actual", FXCollections.observableArrayList());
        XYChart.Series<Number, Number> currentTrajY = new XYChart.Series<>("Y actual", FXCollections.observableArrayList());
               
        for (AcceleratorNode item : accl.getAllNodesOfType("BPM")) {
            refXData.add(new XYChart.Data<>(MainFunctions.mainDocument.DisplayTraj.Pos.get(item), MainFunctions.mainDocument.DisplayTraj.XDiff.get(item)));
            refYData.add(new XYChart.Data<>(MainFunctions.mainDocument.DisplayTraj.Pos.get(item), MainFunctions.mainDocument.DisplayTraj.YDiff.get(item)));
            correctedXData.add(new XYChart.Data<>(MainFunctions.mainDocument.DisplayTraj.Pos.get(item), MainFunctions.mainDocument.DisplayTraj.XDiff.get(item)+ 1e3*(finalPosX.get(item)-iniPosX.get(item))));
            correctedYData.add(new XYChart.Data<>(MainFunctions.mainDocument.DisplayTraj.Pos.get(item), MainFunctions.mainDocument.DisplayTraj.YDiff.get(item)+ 1e3*(finalPosY.get(item)-iniPosY.get(item))));       
        }
        
        correctedTrajX.setData(correctedXData);
        correctedTrajY.setData(correctedYData);
        currentTrajX.setData(refXData);
        currentTrajY.setData(refYData);
                         
        
        chartDataXY.add(currentTrajX);        
        chartDataXY.add(currentTrajY);               
        chartDataXY.add(correctedTrajX);
        chartDataXY.add(correctedTrajY);
               
        chartXY.setData(chartDataXY);
        chartXY.setCreateSymbols(true);  
        chartXY.setHVLSeries(0, 1, -1);    

    }

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public BooleanProperty loggedInProperty() {
        return loggedIn;
    }

    public final boolean isLoggedIn() {
        return loggedInProperty().get();
    }

    public final void setLoggedIn(boolean loggedIn) {
        loggedInProperty().set(loggedIn);
    }

    public void updatePlot1to1() {
        int correctorIndex;
        String plane;
        BPM bpmindex;

        //clear the initial values
        response.getData().clear();
        plane = comboBoxPlane.getValue();
        correctorIndex = comboBoxCorrector.getItems().indexOf(comboBoxCorrector.getValue());
        if (correctorIndex < 0) {
            correctorIndex = 0;
        }
        bpmindex = BPMList.get(correctorIndex);
        ObservableList<XYChart.Data<Number, Number>> Data = FXCollections.observableArrayList();

        for (int i = -20; i < 21; i++) {
            if (plane.equals("Horizontal")) {
                Data.add(new XYChart.Data<>(i, Correction1to1.getHorParam().get(bpmindex)[0] + Correction1to1.getHorParam().get(bpmindex)[1] * i));
            } else if (plane.equals("Vertical")) {
                Data.add(new XYChart.Data<>(i, Correction1to1.getVertParam().get(bpmindex)[0] + Correction1to1.getVertParam().get(bpmindex)[1] * i));
            }
        }
        
        response.setData(Data);

    }

    public void updatePlotSVD() {
        int bpmIndex;
        int correctorIndex;
        int i = 0;
        String plane;

        //referencePlot.getData().clear();
        plane = comboBoxPlane.getValue();
        correctorIndex = comboBoxCorrector.getItems().indexOf(comboBoxCorrector.getValue());
        if (correctorIndex < 0) {
            correctorIndex = 0;
        }
        response.getData().clear();
        ObservableList<XYChart.Data<Number, Number>> Data = FXCollections.observableArrayList();


        for (BPM item : CorrectionSVD.bpm) {
            bpmIndex = CorrectionSVD.bpm.indexOf(item);
            if (plane.equals("Horizontal")) {
                Data.add(new XYChart.Data<>(item.getSDisplay(), CorrectionSVD.TRMhorizontal.get(bpmIndex, correctorIndex)));
            } else if (plane.equals("Vertical")) {
                Data.add(new XYChart.Data<>(item.getSDisplay(), CorrectionSVD.TRMvertical.get(bpmIndex, correctorIndex)));
            }
        }
        
        //Populate the Corrector maps
        List<Double> bpmPos = new ArrayList<>();
        CorrectionSVD.bpm.forEach((item) -> {bpmPos.add(item.getSDisplay());});
               
        ((NumberAxis) chartXY.getXAxis()).setLowerBound(Collections.min(bpmPos));
        ((NumberAxis) chartXY.getYAxis()).setUpperBound(Collections.max(bpmPos));
        
        response.setData(Data);

    }

    @FXML
    private void handleRePlot(ActionEvent event) {
        //if (dataType.equals("SVD")) {
        //    updatePlotSVD();
        //} else 
        if (dataType.equals("1to1")) {
            updatePlot1to1();
        }
    }

    @FXML
    private void handleChangePlane(ActionEvent event) {
        comboBoxCorrector.getItems().clear();
        BPMList.clear();
        if (comboBoxPlane.getValue().equals("Horizontal")) {
            if (dataType.equals("SVD")) {
                CorrectionSVD.HC.forEach(hc -> comboBoxCorrector.getItems().add(hc.toString()));
            } else if (dataType.equals("1to1")) {
                Correction1to1.HC.keySet().forEach(bpm -> BPMList.add(bpm));
                BPMList.sort((bpm1, bpm2) -> Double.compare(bpm1.getSDisplay(), bpm2.getSDisplay()));
                BPMList.forEach(bpm -> comboBoxCorrector.getItems().add(bpm.toString()));
            }
        } else if (comboBoxPlane.getValue().equals("Vertical")) {
            if (dataType.equals("SVD")) {
                CorrectionSVD.VC.forEach(vc -> comboBoxCorrector.getItems().add(vc.toString()));
            } else if (dataType.equals("1to1")) {
                Correction1to1.VC.keySet().forEach(bpm -> BPMList.add(bpm));
                BPMList.sort((bpm1, bpm2) -> Double.compare(bpm1.getSDisplay(), bpm2.getSDisplay()));
                BPMList.forEach(bpm -> comboBoxCorrector.getItems().add(bpm.toString()));
            }
        }
        comboBoxCorrector.setValue(comboBoxCorrector.getItems().get(0));
    }

    @FXML
    private void handleCloseButton(ActionEvent event) {
        setLoggedIn(true);
    }

}
