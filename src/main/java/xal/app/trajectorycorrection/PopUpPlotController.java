/*
 * Copyright (C) 2018 European Spallation Source ERIC
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package xal.app.trajectorycorrection;

/**
 * PopUPPlotController.java
 *
 * Created by Natalia Milas on 07.07.2017
 *
 * Secondary Controller that holds a plot that displays an orbit
 *
 * @author nataliamilas 06-2017
 */
import eu.ess.xaos.ui.plot.LineChartFX;
import eu.ess.xaos.ui.plot.PluggableChartContainer;
import eu.ess.xaos.ui.plot.plugins.Pluggable;
import eu.ess.xaos.ui.plot.plugins.Plugins;
import static java.lang.Math.max;
import static java.lang.Math.min;
import java.util.List;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.chart.ValueAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import xal.smf.impl.BPM;

/**
 * FXML Controller class
 *
 * @author nataliamilas
 */
public class PopUpPlotController {

    private final BooleanProperty loggedIn = new SimpleBooleanProperty();
    //Setup the Plot   
    private final PluggableChartContainer chartContainer = new PluggableChartContainer();
    private LineChartFX<Number, Number> chartXY;
    
    private final XYChart.Series<Number, Number> horizontalSeries = new XYChart.Series<>("horizontal", FXCollections.observableArrayList());
    private final XYChart.Series<Number, Number> verticalSeries = new XYChart.Series<>("vertical", FXCollections.observableArrayList());

    @FXML
    private Button buttonClose;
    @FXML
    private AnchorPane panePlot;

    public BooleanProperty loggedInProperty() {
        return loggedIn;
    }

    public final boolean isLoggedIn() {
        return loggedInProperty().get();
    }

    public final void setLoggedIn(boolean loggedIn) {
        loggedInProperty().set(loggedIn);
    }

    @FXML
    public void handleButtonClose() {
        setLoggedIn(true);

    }

    public void updatePlot(TrajectoryArray Traj) {
        double valMaxY = 0;
        double valMinY = 0;
        double valMaxX = 0;
        double valMinX = 1000;    
        
        horizontalSeries.setName("X [mm]");
        horizontalSeries.getData().clear();
        verticalSeries.setName("Y [mm]");
        verticalSeries.getData().clear();
        
        ObservableList<XYChart.Data<Number, Number>> XData = FXCollections.observableArrayList();
        ObservableList<XYChart.Data<Number, Number>> YData = FXCollections.observableArrayList();

        for (BPM item : Traj.Pos.keySet()) {
            XData.add(new XYChart.Data<>(Traj.Pos.get(item), Traj.XRef.get(item)));
            valMaxY = max(valMaxY, Traj.XRef.get(item));
            valMinY = min(valMinY, Traj.XRef.get(item));
            YData.add(new XYChart.Data<>(Traj.Pos.get(item), Traj.YRef.get(item)));
            valMaxY = max(valMaxY, Traj.YRef.get(item));
            valMinY = min(valMinY, Traj.YRef.get(item));
            valMaxX = max(valMaxX, Traj.Pos.get(item));
            valMinX = min(valMinX, Traj.Pos.get(item));
        }

         //update plot data
        horizontalSeries.setData(XData);
        verticalSeries.setData(YData);
        
        ValueAxis<Number> xAxisXY = new eu.ess.xaos.ui.plot.NumberAxis();
        ValueAxis<Number> yAxisXY = new eu.ess.xaos.ui.plot.NumberAxis();        

        xAxisXY.setLabel("Position [m]");
        yAxisXY.setLabel("Trajectory [mm]");
        
        xAxisXY.setAnimated(false);
        yAxisXY.setAnimated(false);

        chartXY = new LineChartFX<>(xAxisXY, yAxisXY);
        
        chartXY.setAnimated(false);
        chartXY.setOnMouseClicked(event -> chartXY.requestFocus());
        chartXY.addChartPlugins(FXCollections.observableList(List.of(Plugins.all())));
        
        panePlot.getChildren().add(chartContainer);  
        chartContainer.setPluggable((Pluggable) panePlot);        
        
        ObservableList<XYChart.Series<Number, Number>> chartDataXY = FXCollections.observableArrayList();

        chartDataXY.add(horizontalSeries);
        chartDataXY.add(verticalSeries);
       
        chartXY.setData(chartDataXY);
        chartXY.setCreateSymbols(true);
        
        
    }

}
