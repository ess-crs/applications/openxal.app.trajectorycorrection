/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xal.app.trajectorycorrection;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import xal.ca.BatchConnectionRequest;
import xal.ca.Channel;
import xal.ca.ChannelRecord;
import xal.ca.ConnectionListener;
import xal.ca.IEventSinkValue;
import xal.ca.MonitorException;
import xal.smf.impl.BPM;

/**
 *
 * @author nataliamilas
 */
class ChannelMonitor implements IEventSinkValue, ConnectionListener {

    private BatchConnectionRequest request;
    private HashMap<Channel,List<Double>> inputChannels;
    private Integer AverageNumber = 1;
    private xal.ca.Monitor monitor;    
    private BooleanProperty updateAvg = new SimpleBooleanProperty();
    private HashMap<Channel,Boolean> channelUpdate = new HashMap();
    
    
    public ChannelMonitor() {
       this.inputChannels= new HashMap<>();       

    }        

    public void connectAndMonitor(List<BPM> BPMList, int Navg) {       
        
        this.AverageNumber = Navg;
                
        BPMList.parallelStream().forEach(bpm-> {
            inputChannels.put(bpm.getChannel("xAvg"),new ArrayList<Double>());
            inputChannels.put(bpm.getChannel("yAvg"),new ArrayList<Double>());            
        });
        
        
        List<Channel> channelList = new ArrayList<Channel>();
        channelList.addAll(inputChannels.keySet());
        
        request = new BatchConnectionRequest(channelList);
        request.submitAndWait(5.0);  
        
        channelList.forEach(channel->{
            channel.addConnectionListener(this);            
        });        
    }
    
    public void disconnectAndClearAll(){
        inputChannels.keySet().forEach(channel->{
            channel.removeConnectionListener(this);
            monitor.clear();
        });
        inputChannels.clear();                        
    }        

    public BooleanProperty getUpdateAvg(){
        return updateAvg;
    }

    @Override
    public void eventValue(ChannelRecord record, Channel chan) {
        if (chan.isConnected()) {            
            Platform.runLater(
            () -> {
                if (inputChannels.keySet().contains(chan)){
                    if(inputChannels.get(chan).size()<AverageNumber)
                        inputChannels.get(chan).add(record.doubleValue());
                } else if (inputChannels.get(chan).size()>=AverageNumber){
                    channelUpdate.put(chan, true);
                }                
                checkUpdate();                
            });                
            } 
    }

    @Override
    public void connectionMade(Channel channel) {
        if (channel.isConnected()) {
            try {            
                monitor = channel.addMonitorValue(this, xal.ca.Monitor.VALUE); 
                channelUpdate.put(channel,false);
            } catch (MonitorException ex) {
                Logger.getLogger(ChannelMonitor.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public void connectionDropped(Channel chan) {       
        monitor.clear();
        channelUpdate.remove(chan);
    }
    
    public void checkUpdate(){
        if (!channelUpdate.values().contains(false)){
            updateAvg.set(true);
            disconnectAndClearAll();
        } else {
            updateAvg.set(false);
        }       
    }   
    
}  
