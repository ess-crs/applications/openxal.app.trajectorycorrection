/*
 * Copyright (C) 2018 European Spallation Source ERIC
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package xal.app.trajectorycorrection;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ToggleGroup;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.scene.control.RadioButton;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.control.Tab;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.ToolBar;
import javafx.scene.image.ImageView;
import javafx.scene.input.ContextMenuEvent;
import javafx.scene.layout.AnchorPane;
import xal.ca.ConnectionException;
import xal.ca.GetException;
import xal.ca.MonitorException;
import xal.ca.PutException;
import xal.extension.fxapplication.Controller;
import xal.smf.impl.BPM;
import xal.smf.impl.DipoleCorr;
import xal.tools.data.DataAdaptor;
import xal.tools.xml.XmlDataAdaptor;

public class FXMLController extends Controller {


    //radio buttons & groups
    @FXML
    private ToggleGroup groupCorrection;
    @FXML
    private RadioButton radiobuttonCorrectSVD;
    @FXML
    private RadioButton radiobuttonCorrect1to1;
    @FXML
    private ToggleGroup groupCorrectionPlane;
    @FXML
    private RadioButton radioButtonVertical;
    @FXML
    private RadioButton radioButtonHorizontal;
    @FXML
    private RadioButton radioButtonHorVer;

    @FXML
    private Button buttonMeasureResponse1to1;
    @FXML
    private Button buttonMeasureResponseSVD;
    @FXML
    private Button buttonCalcCorrectSVD;
    @FXML
    private Button buttonPlotRefTraj;
    @FXML
    private Button buttonSVDfromModel;
    @FXML
    private Button buttonVerifyMatrixSVD;
    @FXML
    private Button button1to1fromModel;
    @FXML
    private Button buttonVerifyResponse1to1;
    @FXML
    private Button buttonCorrectTrajectory;

    //Text field & labels
    @FXML
    private TextField textFieldSingularValCut;
    @FXML
    private TextArea textArea1to1;
    @FXML
    private TextArea textAreaSVD;
    @FXML
    private Label labelProgressCorrection;
    @FXML
    private TextField textFieldCorrectionFactor;

    //progess bar
    @FXML
    private ProgressBar progressBarCorrection;

    //others
    @FXML
    private ComboBox<String> comboBoxRefTrajectory;
    @FXML
    private ListView<CorrectionBlock> listViewBlockDefinition;
    @FXML
    private ListView<CorrectionBlock> listViewBlockSelection;

    //Variables   
    private boolean abortFlag = false;
    @FXML
    private AnchorPane mainAnchor1;
    @FXML
    private ToolBar bottomBar;
    @FXML
    private Label labelExpert;
    @FXML
    private Button buttonAbort;
    @FXML
    private Button buttonCheckPair;
    @FXML
    private Tab tabSVD;
    @FXML
    private Tab tab1to1;
    @FXML
    private TextField textFieldTrajectoryAvg;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
               
        //initialize and connect BPMs
        MainFunctions.mainDocument.DisplayTraj.initBPMs(MainFunctions.mainDocument.getAccelerator());

        //initalize horizontal correctors
        MainFunctions.mainDocument.getAccelerator().getAllNodesOfType("DCH").parallelStream().forEach(hc -> {
            if (!(hc.getChannel("I_Set").isConnected())) {
                hc.getChannel("I_Set").connectAndWait(1.0);
            }
        });

        //initalize vertical correctors
        MainFunctions.mainDocument.getAccelerator().getAllNodesOfType("DCV").parallelStream().forEach(hv -> {
            if (!(hv.getChannel("I_Set").isConnected())) {
                hv.getChannel("I_Set").connectAndWait(1.0);
            }
        });

        //Set elements not visible at start
        labelProgressCorrection.setVisible(false);
        progressBarCorrection.setVisible(false);

        //set css id for listview
        listViewBlockDefinition.setId("listBlockDefinition");
        listViewBlockSelection.setId("listBlockSelection");

        //Sort elements
        MainFunctions.mainDocument.CorrectionElements.sort((ele1, ele2) -> Double.compare(ele1.getBlockBPM().get(0).getSDisplay(), ele2.getBlockBPM().get(0).getSDisplay()));
        MainFunctions.mainDocument.CorrectionElementsSelected.sort((ele1, ele2) -> Double.compare(ele1.getBlockBPM().get(0).getSDisplay(), ele2.getBlockBPM().get(0).getSDisplay()));
        
        MainFunctions.mainDocument.getAcceleratorProperty().addChangeListener((ChangeListener) (ObservableValue o, Object oldVal, Object newVal) -> {
            //initialize and connect BPMs
            MainFunctions.mainDocument.DisplayTraj.initBPMs(MainFunctions.mainDocument.getAccelerator());

            //initalize horizontal correctors
            MainFunctions.mainDocument.getAccelerator().getAllNodesOfType("DCH").parallelStream().forEach(hc -> {
                if (!(hc.getChannel("I_Set").isConnected())) {
                    hc.getChannel("I_Set").connectAndWait(1.0);
                }
            });

            //initalize vertical correctors
            MainFunctions.mainDocument.getAccelerator().getAllNodesOfType("DCV").parallelStream().forEach(hv -> {
                if (!(hv.getChannel("I_Set").isConnected())) {
                    hv.getChannel("I_Set").connectAndWait(1.0);
                }
            });
        });

        listViewBlockDefinition.setItems(MainFunctions.mainDocument.CorrectionElements);
        listViewBlockDefinition.setCellFactory(listview -> {
            return new ListCell<CorrectionBlock>() {
                @Override
                public void updateItem(CorrectionBlock item, boolean empty) {
                    super.updateItem(item, empty);
                    textProperty().unbind();
                    if (item != null) {
                        textProperty().bind(item.blockNameProperty());
                    } else {
                        setText(null);
                    }
                }
            };
        }
        );

        listViewBlockDefinition.setEditable(true);

        listViewBlockDefinition.setOnEditCommit((ListView.EditEvent<CorrectionBlock> t) -> {
            Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("Information Dialog");
            alert.setHeaderText(null);
            alert.setContentText("A Block with this name already exists!");
            MainFunctions.mainDocument.CorrectionElements.forEach(block -> {
                if (block.getblockName().equals(t.getNewValue().getblockName())) {
                    alert.show();
                }
            });
            MainFunctions.mainDocument.CorrectionElementsSelected.forEach(block -> {
                if (block.getblockName().equals(t.getNewValue().getblockName())) {
                    alert.show();
                }
            });
        });

        listViewBlockSelection.setItems(MainFunctions.mainDocument.CorrectionElementsSelected);
        listViewBlockSelection.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            if (newSelection != null) {
                if (newSelection.isOkSVD()) {
                    tabSVD.setGraphic(new ImageView(this.getClass().getResource("/pictures/ok.png").toString()));
                } else {
                    tabSVD.setGraphic(new ImageView(this.getClass().getResource("/pictures/error.png").toString()));
                }
                if (newSelection.isOk1to1()) {
                    tab1to1.setGraphic(new ImageView(this.getClass().getResource("/pictures/ok.png").toString()));
                } else {
                    tab1to1.setGraphic(new ImageView(this.getClass().getResource("/pictures/error.png").toString()));
                }
            }
        });

        //tab1to1.contentProperty().addListener(listener);
        //listViewBlockSelection.setOnMouseClicked(new EventHandler<MouseEvent>() {
        //    @Override
        //    public void handle(MouseEvent event) {
        //        CorrectionBlock item = listViewBlockSelection.getSelectionModel().getSelectedItem();
        //        if (item.isOkSVD()){
        //           tabSVD.setGraphic(new ImageView(this.getClass().getResource("/pictures/ok.png").toString()));
        //        } else {
        //           tabSVD.setGraphic(new ImageView(this.getClass().getResource("/pictures/error.png").toString()));
        //        }
        //        if (item.isOk1to1()){
        //           tab1to1.setGraphic(new ImageView(this.getClass().getResource("/pictures/ok.png").toString()));
        //        } else {
        //           tab1to1.setGraphic(new ImageView(this.getClass().getResource("/pictures/error.png").toString()));
        //        }
        //    }
        //});
        listViewBlockSelection.setCellFactory(lv -> new ListCell<CorrectionBlock>() {
            @Override
            protected void updateItem(CorrectionBlock item, boolean empty) {
                super.updateItem(item, empty);
                if (empty || item == null) {
                    setText(null);
                    setGraphic(null);
                } else {
                    setText(item.getblockName());
                    if (item.isOkSVD() && item.isOk1to1()) {
                        setGraphic(new ImageView(this.getClass().getResource("/pictures/ok.png").toString()));
                    } else {
                        setGraphic(new ImageView(this.getClass().getResource("/pictures/error.png").toString()));
                    }
                }
            }
        });

        listViewBlockSelection.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends CorrectionBlock> observable, CorrectionBlock oldValue, CorrectionBlock newValue) -> {
            List<BPM> BPMList = new ArrayList<>();
            int maxBPM = 0;
            BPMList.addAll(MainFunctions.mainDocument.getAccelerator().getAllNodesOfType("BPM"));
            try {
                MainFunctions.mainDocument.DisplayTraj.readTrajectory(BPMList);
            } catch (ConnectionException | GetException ex) {
                Logger.getLogger(FXMLController.class.getName()).log(Level.SEVERE, null, ex);
            }

            if (MainFunctions.mainDocument.CorrectionElementsSelected.contains(newValue)) {
                if (newValue.isOk1to1()) {
                    buttonCheckPair.setDisable(false);
                    buttonMeasureResponse1to1.setDisable(false);
                    button1to1fromModel.setDisable(false);
                    buttonVerifyResponse1to1.setDisable(false);
                } else {
                    buttonCheckPair.setDisable(false);
                    buttonMeasureResponse1to1.setDisable(true);
                    button1to1fromModel.setDisable(true);
                    buttonVerifyResponse1to1.setDisable(true);
                }
                if (newValue.isOkSVD()) {
                    buttonSVDfromModel.setDisable(false);
                    buttonVerifyMatrixSVD.setDisable(false);
                    buttonCalcCorrectSVD.setDisable(false);
                    if (Math.abs(MainFunctions.mainDocument.DisplayTraj.getXmax()) < MainFunctions.mainDocument.getTrajExpertLimit() && Math.abs(MainFunctions.mainDocument.DisplayTraj.getYmax()) < MainFunctions.mainDocument.getTrajExpertLimit()) {
                        buttonMeasureResponseSVD.setDisable(true);
                    } else {
                        buttonMeasureResponseSVD.setDisable(false);
                    }
                } else {
                    if (Math.abs(MainFunctions.mainDocument.DisplayTraj.getXmax()) < MainFunctions.mainDocument.getTrajExpertLimit() && Math.abs(MainFunctions.mainDocument.DisplayTraj.getYmax()) < MainFunctions.mainDocument.getTrajExpertLimit()) {
                        buttonMeasureResponseSVD.setDisable(true);
                    } else {
                        buttonMeasureResponseSVD.setDisable(false);
                    }
                    buttonSVDfromModel.setDisable(false);
                    buttonVerifyMatrixSVD.setDisable(true);
                    buttonCalcCorrectSVD.setDisable(true);
                }
            } else {
                buttonCheckPair.setDisable(true);
                buttonMeasureResponse1to1.setDisable(true);
                button1to1fromModel.setDisable(true);
                buttonVerifyResponse1to1.setDisable(true);
                buttonMeasureResponseSVD.setDisable(true);
                buttonSVDfromModel.setDisable(true);
                buttonVerifyMatrixSVD.setDisable(true);
                buttonCalcCorrectSVD.setDisable(true);
            }
        });

        //Load reference and zero trajectories
        //comboBoxRefTrajectory.setCellFactory((ListView<URL> fileName) -> {
        //    ListCell cell = new ListCell<URL>() {
        //        @Override
        //        protected void updateItem(URL item, boolean empty) {
        //            super.updateItem(item, empty);
        //            if (empty || item == null) {
        //                setText("");
        //            } else {
        //                setText(new File(item.getFile()).getName());
        //            }
        //        }
        //    };
        //    return cell;
        //});

        //Load reference and zero trajectories
        //MainFunctions.mainDocument.refTrajData.add(this.getClass().getResource("/zerotrajectory/ZeroTrajectory.xml"));

        //populate the ComboBox element
        //comboBoxRefTrajectory.setItems(MainFunctions.mainDocument.refTrajData);
        //comboBoxRefTrajectory.setValue(MainFunctions.mainDocument.refTrajData.get(0));
        //populate the ComboBox element
        comboBoxRefTrajectory.getItems().addAll("Zero Trajectory");
        comboBoxRefTrajectory.setValue("Zero Trajectory");       

        //read new reference trajectory file
        //try {
        //MainFunctions.mainDocument.DisplayTraj.readReferenceTrajectory(accl, comboBoxRefTrajectory.getSelectionModel().getSelectedItem());
        //} catch (IOException ex) {
        //    Logger.getLogger(FXMLController.class.getName()).log(Level.SEVERE, null, ex);
        //}

        try {
            MainFunctions.mainDocument.DisplayTraj.readReferenceTrajectory(MainFunctions.mainDocument.getAccelerator());            
        } catch (ConnectionException ex) {
            Logger.getLogger(FXMLController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (GetException ex) {
            Logger.getLogger(FXMLController.class.getName()).log(Level.SEVERE, null, ex);
        }
                
    }
    
    

    //private void updateExpertLabel() {
    //    Logger.getLogger(FXMLController.class.getName()).log(Level.FINER, "Updating expert label triggered");
    //    if (compareNumbers(trajectoryLimit.get(), defaultTrajectoryLimit)) {
    //        labelExpert.setVisible(true);
    //        bottomBar.setStyle("-fx-background-color: #ff0000;");
    //        if (compareNumbers(steererLimit.get(), defaultSteererLimit)) {
    //            labelExpert.setText("Steerer and Trajectory Limit Modified");
    //        } else {
    //            labelExpert.setText("Trajectory Limit Modified");
    //        }
    //    } else if (compareNumbers(steererLimit.get(), defaultSteererLimit)) {
    //        labelExpert.setVisible(true);
    //        bottomBar.setStyle("-fx-background-color: #ff0000;");
    //        labelExpert.setText("Steerer Limit Modified");
    //    } else {
    //        bottomBar.setStyle(null);
    //        labelExpert.setVisible(false);
    //    }
    //}

    //public boolean compareNumbers(double value, double default_value, double limit) {
    //    if (Math.abs((value - default_value) / default_value) > limit) {
    //        return true;
    //    }
    //    return false;
    //}

    //public boolean compareNumbers(double value, double default_value) {
    //    return compareNumbers(value, default_value, 1e-4);
    ///}    

    @FXML
    private void handleCheckPairs(ActionEvent event) {
        int blockIndex = listViewBlockSelection.getSelectionModel().getSelectedIndex();

        if (listViewBlockSelection.getSelectionModel().getSelectedItem() != null) {
            MainFunctions.mainDocument.CorrectionElementsSelected.get(blockIndex).check1to1CorrectionPairs(listViewBlockDefinition.getScene().getWindow(), MainFunctions.mainDocument.getAccelerator());
            //print pairs to text area
            textArea1to1.clear();
            textArea1to1.setText("Horizontal pairs: \n");
            MainFunctions.mainDocument.CorrectionElementsSelected.get(blockIndex).getCorrection1to1().getHC().keySet().stream().forEach(bpm -> textArea1to1.setText(textArea1to1.getText() + bpm.toString() + " : " + MainFunctions.mainDocument.CorrectionElementsSelected.get(blockIndex).getCorrection1to1().getHC().get(bpm).toString() + "\n"));
            textArea1to1.setText(textArea1to1.getText() + "Vertical pairs: \n");
            MainFunctions.mainDocument.CorrectionElementsSelected.get(blockIndex).getCorrection1to1().getVC().keySet().stream().forEach(bpm -> textArea1to1.setText(textArea1to1.getText() + bpm.toString() + " : " + MainFunctions.mainDocument.CorrectionElementsSelected.get(blockIndex).getCorrection1to1().getVC().get(bpm).toString() + "\n"));
            
            if (MainFunctions.mainDocument.CorrectionElementsSelected.get(blockIndex).isOk1to1()) {
                button1to1fromModel.setDisable(false);
                buttonMeasureResponse1to1.setDisable(false);
                buttonVerifyResponse1to1.setDisable(false);
                tab1to1.setGraphic(new ImageView(getClass().getResource("/pictures/ok.png").toString()));
            } else {
                button1to1fromModel.setDisable(false);
                buttonMeasureResponse1to1.setDisable(false);
                buttonVerifyResponse1to1.setDisable(true);
                tab1to1.setGraphic(new ImageView(getClass().getResource("/pictures/error.png").toString()));

            }
            listViewBlockSelection.refresh();
        }        

    }

    @FXML
    private void handleMeasureResponse1to1(ActionEvent event) {

        TextInputDialog dialog = new TextInputDialog("0.002");
        dialog.setTitle("Set Field Variation");
        dialog.setHeaderText(null);
        dialog.setContentText("Enter Corrector strength (T.m) step:");
        Optional<String> result = dialog.showAndWait();
        double resultval = 0.002;
        if (result.isPresent()) {
            resultval = Double.parseDouble(result.get());
            if (resultval <= 0.0 || resultval > 0.01) {
                resultval = 0.002;
            }
        }
        int blockIndex = listViewBlockSelection.getSelectionModel().getSelectedIndex();

        final double Dk = resultval;
        Task<Void> task;
        task = new Task<Void>() {

            @Override
            protected Void call() throws Exception {

                int progress = 0;
                int total = MainFunctions.mainDocument.CorrectionElementsSelected.get(blockIndex).getCorrection1to1().getHC().size() + MainFunctions.mainDocument.CorrectionElementsSelected.get(blockIndex).getCorrection1to1().getVC().size();
                //make horizontal calibration for each bpm
                for (BPM bpm : MainFunctions.mainDocument.CorrectionElementsSelected.get(blockIndex).getCorrection1to1().getHC().keySet()) {
                    MainFunctions.mainDocument.CorrectionElementsSelected.get(blockIndex).getCorrection1to1().getHCalibration(bpm, Dk);
                    //update progressbar
                    progress++;
                    updateProgress(progress, total);
                }

                //make vertical calibration for each bpm
                for (BPM bpm : MainFunctions.mainDocument.CorrectionElementsSelected.get(blockIndex).getCorrection1to1().getVC().keySet()) {
                    MainFunctions.mainDocument.CorrectionElementsSelected.get(blockIndex).getCorrection1to1().getVCalibration(bpm, Dk);
                    //update progressbar
                    progress++;
                    updateProgress(progress, total);
                }

                MainFunctions.mainDocument.CorrectionElementsSelected.get(blockIndex).setOk1to1(true);
                //when the scan finishes set the label and progress bar to zero
                labelProgressCorrection.setVisible(false);
                progressBarCorrection.setVisible(false);
                updateProgress(0, total);
                textArea1to1.setText("RESPONSE MEASUREMENT: Finished!");
                listViewBlockSelection.refresh();
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        tab1to1.setGraphic(new ImageView(getClass().getResource("/pictures/ok.png").toString()));
                    }
                });
                //buttonCheckPair.setDisable(false);
                //buttonMeasureResponse1to1.setDisable(false);
                //button1to1fromModel.setDisable(false);
                buttonVerifyResponse1to1.setDisable(false);

                return null;
            }
        ;

        };

        Thread calibrate = new Thread(task);
        calibrate.setDaemon(true); // thread will not prevent application shutdown
        if (result.isPresent()) {
            labelProgressCorrection.setVisible(true);
            progressBarCorrection.setVisible(true);
            labelProgressCorrection.setText("Aquiring BPM responses");
            progressBarCorrection.progressProperty().bind(task.progressProperty());
            calibrate.start();
        }

    }

    private void correct1to1() {

        Task<Void> task;
        task = new Task<Void>() {

            @Override
            protected Void call() throws Exception {

                double DeltaK = 0.0;
                double val = 0.0;
                double correctFactor = Double.parseDouble(textFieldCorrectionFactor.getText()) / 100;
                int numberAvg = Integer.parseInt(textFieldTrajectoryAvg.getText());
                int total = 0;
                int step = 0;
                CorrectionMatrix CorrectTraj = new CorrectionMatrix();
                
                if (numberAvg <=0){
                    numberAvg = 1;
                }

                for (CorrectionBlock block : MainFunctions.mainDocument.CorrectionElementsSelected) {
                    CorrectTraj = block.getCorrection1to1();

                    if (radioButtonHorizontal.isSelected()) {
                        total += CorrectTraj.getHC().size();
                    } else if (radioButtonVertical.isSelected()) {
                        total += CorrectTraj.getVC().size();
                    } else if (radioButtonHorVer.isSelected()) {
                        total += CorrectTraj.getVC().size() + CorrectTraj.getHC().size();
                    }
                }

                for (CorrectionBlock block : MainFunctions.mainDocument.CorrectionElementsSelected) {
                    CorrectTraj = block.getCorrection1to1();

                    List<BPM> BPMList = block.getBlockBPM();

                    labelProgressCorrection.setText("Correcting Trajectory: " + block.getblockName());
                    //correct trajectory
                    for (BPM item : BPMList) {
                        //read averaged trajecotry for the given BPM 
                        try {  
                          MainFunctions.mainDocument.DisplayTraj.readTrajectory(Arrays.asList(item));  
                          //MainFunctions.mainDocument.DisplayTraj.readAvgTrajectory(Arrays.asList(item), numberAvg);                                    
                        } catch (ConnectionException | GetException ex) {
                           Logger.getLogger(FXMLController.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        if (radioButtonHorizontal.isSelected() || radioButtonHorVer.isSelected()) {
                            if (CorrectTraj.HC.containsKey(item) && !abortFlag) {                                
                                val = CorrectTraj.HC.get(item).getCurrent();                                                                                                                              
                                DeltaK = correctFactor * CorrectTraj.HC.get(item).toCurrentFromField(CorrectTraj.calcHCorrection(item));
                                val = val + DeltaK;
                                if (Math.abs(val) > block.getMaxCorr()){
                                    if (val > 0){
                                        val = block.getMaxCorr();
                                    } else {
                                        val = -1*block.getMaxCorr();
                                    }
                                }
                                CorrectTraj.HC.get(item).setCurrent(val);
                                Thread.sleep(2000);
                                step++;
                                updateProgress(step, total);
                            }
                        }
                        if (radioButtonHorVer.isSelected() || radioButtonVertical.isSelected()) {
                            if (CorrectTraj.VC.containsKey(item) && !abortFlag) {
                                val = CorrectTraj.VC.get(item).getCurrent();
                                DeltaK = correctFactor * CorrectTraj.VC.get(item).toCurrentFromField(CorrectTraj.calcVCorrection(item));;
                                val = val + DeltaK;
                                if (Math.abs(val) > block.getMaxCorr()){
                                    if (val > 0){
                                        val = block.getMaxCorr();
                                    } else {
                                        val = -1*block.getMaxCorr();
                                    }
                                }
                                CorrectTraj.VC.get(item).setCurrent(val);
                                Thread.sleep(2000);
                                step++;
                                updateProgress(step, total);
                            }
                        }
                    }
                }
                //when the scan finishes set the label and progress bar to zero
                labelProgressCorrection.setVisible(false);
                progressBarCorrection.setVisible(false);
                updateProgress(0, total);

                return null;
            }
        ;

        };

        labelProgressCorrection.setVisible(true);
        progressBarCorrection.setVisible(true);
        labelProgressCorrection.setText("Correcting Trajectory");
        progressBarCorrection.progressProperty().bind(task.progressProperty());
        Thread correct = new Thread(task);
        correct.setDaemon(true); // thread will not prevent application shutdown
        correct.start();

    }

    @FXML
    private void handleMeasureResponseSVD(ActionEvent event) {

        int blockIndex = listViewBlockSelection.getSelectionModel().getSelectedIndex();

        if (!listViewBlockSelection.getSelectionModel().getSelectedItem().getblockName().isEmpty()) {

            TextInputDialog dialog = new TextInputDialog("0.002");
            dialog.setTitle("Set Field Variation");
            dialog.setHeaderText(null);
            dialog.setContentText("Enter Corrector strength (T.m) step:");
            Optional<String> result = dialog.showAndWait();
            double resultval = 0.002;
            if (result.isPresent()) {
                resultval = Double.parseDouble(result.get());
                if (resultval <= 0.0 || resultval > 0.01) {
                    resultval = 0.002;
                }
            }

            final double Dk = resultval;

            //print knobs
            textAreaSVD.clear();

            //MainFunctions.mainDocument.CorrectionElementsSelected.get(blockName).initializeSVDCorrection(MainFunctions.mainDocument.getAccelerator());
            textAreaSVD.setText(textAreaSVD.getText() + MainFunctions.mainDocument.CorrectionElementsSelected.get(blockIndex).getblockName() + "\n");
            textAreaSVD.setText(textAreaSVD.getText() + "Horizontal Correctors: \n");
            MainFunctions.mainDocument.CorrectionElementsSelected.get(blockIndex).getBlockHC().forEach(hcorr -> textAreaSVD.setText(textAreaSVD.getText() + hcorr.toString() + "; "));
            textAreaSVD.setText(textAreaSVD.getText() + "\n");
            textAreaSVD.setText(textAreaSVD.getText() + "Vertical correctors: \n");
            MainFunctions.mainDocument.CorrectionElementsSelected.get(blockIndex).getBlockVC().forEach(vcorr -> textAreaSVD.setText(textAreaSVD.getText() + vcorr.toString() + "; "));
            textAreaSVD.setText(textAreaSVD.getText() + "\n");

            Task<Void> task;
            task = new Task<Void>() {

                @Override
                protected Void call() throws Exception {

                    int progress = 0;
                    MainFunctions.mainDocument.CorrectionElementsSelected.get(blockIndex).getCorrectionSVD().measureTRMHorizontal(Dk);
                    updateProgress(1, 2);
                    MainFunctions.mainDocument.CorrectionElementsSelected.get(blockIndex).getCorrectionSVD().measureTRMVertical(Dk);
                    MainFunctions.mainDocument.CorrectionElementsSelected.get(blockIndex).setOkSVD(true);
                    progress++;
                    updateProgress(2, 2);

                    //when the scan finishes set the label and progress bar to zero
                    labelProgressCorrection.setVisible(false);
                    progressBarCorrection.setVisible(false);
                    updateProgress(0, 2);
                    textAreaSVD.setText("RESPONSE MEASUREMENT: Finished!");
                    buttonVerifyMatrixSVD.setDisable(false);
                    buttonCalcCorrectSVD.setDisable(false);
                    listViewBlockSelection.refresh();
                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            tabSVD.setGraphic(new ImageView(getClass().getResource("/pictures/ok.png").toString()));
                        }
                    });
                    try {
                        MainFunctions.mainDocument.DisplayTraj.readTrajectory(MainFunctions.mainDocument.getAccelerator().getAllNodesOfType("BPM"));
                    } catch (ConnectionException | GetException ex) {
                        Logger.getLogger(FXMLController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    buttonSVDfromModel.setDisable(false);
                    buttonVerifyMatrixSVD.setDisable(false);
                    buttonCalcCorrectSVD.setDisable(false);
                    if (Math.abs(MainFunctions.mainDocument.DisplayTraj.getXmax()) < MainFunctions.mainDocument.getTrajExpertLimit() && Math.abs(MainFunctions.mainDocument.DisplayTraj.getYmax()) < MainFunctions.mainDocument.getTrajExpertLimit()) {
                        buttonMeasureResponseSVD.setDisable(true);
                    } else {
                        buttonMeasureResponseSVD.setDisable(false);
                    }

                    return null;
                }
            ;

            };

            Thread calibrate = new Thread(task);
            calibrate.setDaemon(true); // thread will not prevent application shutdown
            progressBarCorrection.progressProperty().bind(task.progressProperty());
            if (result.isPresent()) {
                labelProgressCorrection.setVisible(true);
                progressBarCorrection.setVisible(true);
                labelProgressCorrection.setText("Measuring response matrix...");
                calibrate.start();
            }
        } else {
            Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("Information Dialog");
            alert.setHeaderText(null);
            alert.setContentText("Please select a correction block in the Selected column to measure.");
            alert.show();
        }

    }

    @FXML
    private void handleCalculateCorrectionSVD(ActionEvent event) throws IOException {

        double[] kickH;
        double[] kickV;
        double[] singVal;
        double svCut;
        int total = 0;
        double correctionFactor;
        int i = 0;

        correctionFactor = Double.parseDouble(textFieldCorrectionFactor.getText()) / 100;
        svCut = Double.parseDouble(textFieldSingularValCut.getText());

        textAreaSVD.clear();

        for (CorrectionBlock block : MainFunctions.mainDocument.CorrectionElementsSelected) {
            CorrectionSVD matrix = new CorrectionSVD();
            matrix = block.getCorrectionSVD();
            //reads a new trajectory
            try {
                MainFunctions.mainDocument.DisplayTraj.readTrajectory(matrix.bpm);
                MainFunctions.mainDocument.DisplayTraj.readReferenceTrajectory(MainFunctions.mainDocument.getAccelerator(), comboBoxRefTrajectory.getSelectionModel().getSelectedItem());
            } catch (ConnectionException | GetException ex) {
                Logger.getLogger(FXMLController.class.getName()).log(Level.SEVERE, null, ex);
            }
            matrix.setCutSVD(svCut);
            kickH = matrix.calculateHCorrection(MainFunctions.mainDocument.DisplayTraj);
            kickV = matrix.calculateVCorrection(MainFunctions.mainDocument.DisplayTraj);

            textAreaSVD.setText(textAreaSVD.getText() + "Delta Current Horizontal Corrector: \n");
            i = 0;
            for (DipoleCorr hcorr : matrix.HC) {
                textAreaSVD.setText(textAreaSVD.getText() + hcorr.toString() + " : " + String.format("%.5f", correctionFactor * hcorr.toCurrentFromField(kickH[i])) + "\n");
                i++;
            }
            textAreaSVD.setText(textAreaSVD.getText() + "Delta Current Vertical Corrector: \n");
            i = 0;
            for (DipoleCorr vcorr : matrix.VC) {
                textAreaSVD.setText(textAreaSVD.getText() + vcorr.toString() + " : " + String.format("%.5f", correctionFactor * vcorr.toCurrentFromField(kickV[i])) + "\n");
                i++;
            }

            singVal = matrix.getSigularValuesH();
            textAreaSVD.setText(textAreaSVD.getText() + "Horizontal Singular Values \n");
            for (int j = 0; j < singVal.length; j++) {
                textAreaSVD.setText(textAreaSVD.getText() + String.format("%.3f", singVal[j]) + ";");
            }
            textAreaSVD.setText(textAreaSVD.getText() + "\n");
            for (int j = 0; j < singVal.length; j++) {
                if (singVal[j] > matrix.getCutSVD()) {
                    total += 1;
                }
            }
            textAreaSVD.setText(textAreaSVD.getText() + "Number of horizontal singular values used: " + total + "\n");
            total = 0;
            singVal = matrix.getSigularValuesV();
            textAreaSVD.setText(textAreaSVD.getText() + "Vertical Singular Values \n");
            for (int j = 0; j < singVal.length; j++) {
                textAreaSVD.setText(textAreaSVD.getText() + String.format("%.3f", singVal[j]) + ";");
            }
            textAreaSVD.setText(textAreaSVD.getText() + "\n");
            for (int j = 0; j < singVal.length; j++) {
                if (singVal[j] > matrix.getCutSVD()) {
                    total += 1;
                }
            }
            textAreaSVD.setText(textAreaSVD.getText() + "Number of vertical singular values used: " + total + "\n");

        }

    }

    private void correctSVD() throws PutException, MonitorException, InterruptedException, GetException {

        double[] kickH;
        double[] kickV;
        double svCut;
        double correctionFactor;
        int numberAvg;
        List<DipoleCorr> HC = new ArrayList<>();
        List<DipoleCorr> VC = new ArrayList<>();
        int i = 0;
        int j = 0;

        correctionFactor = Double.parseDouble(textFieldCorrectionFactor.getText()) / 100;
        numberAvg = Integer.parseInt(textFieldTrajectoryAvg.getText());
        svCut = Double.parseDouble(textFieldSingularValCut.getText());
        
        //cross check for number larger than zero
        if (numberAvg<0){
            numberAvg = 1;
        }

        labelProgressCorrection.setVisible(true);        
        progressBarCorrection.setVisible(true);   
        progressBarCorrection.progressProperty().unbind();
        progressBarCorrection.setProgress(0);

        for (CorrectionBlock block : MainFunctions.mainDocument.CorrectionElementsSelected) {
            CorrectionSVD matrix = new CorrectionSVD();
            matrix = block.getCorrectionSVD();
            labelProgressCorrection.setText("Correcting Trajectory: " + block.getblockName());
            //reads a new trajectory
            try {
                MainFunctions.mainDocument.DisplayTraj.readTrajectory(matrix.bpm);
                //MainFunctions.mainDocument.DisplayTraj.readAvgTrajectory(matrix.bpm, numberAvg);                
            } catch (ConnectionException | GetException ex) {
                Logger.getLogger(FXMLController.class.getName()).log(Level.SEVERE, null, ex);
            }
            matrix.setCutSVD(svCut);
            kickH = matrix.calculateHCorrection(MainFunctions.mainDocument.DisplayTraj);
            kickV = matrix.calculateVCorrection(MainFunctions.mainDocument.DisplayTraj);

            //Check steerer values
            double maxkickH = 0.0;//Math.abs(Arrays.stream(kickH).map(n -> Math.abs(n)).max().orElse(0.0));
            double maxkickV = 0.0;//Math.abs(Arrays.stream(kickV).map(n -> Math.abs(n)).max().orElse(0.0));
                                    
            textAreaSVD.clear();
            textAreaSVD.setText(textAreaSVD.getText() + "Final Horizontal Corrector Values: \n");
            i = 0;
            for (DipoleCorr hcorr : matrix.HC) {
                textAreaSVD.setText(textAreaSVD.getText() + hcorr.toString() + " : " + String.format("%.5f", hcorr.getCurrent()+ correctionFactor * hcorr.toCurrentFromField(kickH[i])) + "\n");
                i++;
            }
            textAreaSVD.setText(textAreaSVD.getText() + "Final Vertical Corrector Values: \n");
            i = 0;
            for (DipoleCorr vcorr : matrix.VC) {
                textAreaSVD.setText(textAreaSVD.getText() + vcorr.toString() + " : " + String.format("%.5f", vcorr.getCurrent() + correctionFactor * vcorr.toCurrentFromField(kickV[i])) + "\n");
                i++;
            }

            if (maxkickH < MainFunctions.mainDocument.getSteerersExpertLimit() && maxkickV < MainFunctions.mainDocument.getSteerersExpertLimit()) {
                double aux = 0;
                i = 0;
                if (radioButtonHorizontal.isSelected() || radioButtonHorVer.isSelected()) {
                    for (DipoleCorr hcorr : matrix.HC) {
                        if (!abortFlag) {
                            try {
                                //aux = hcorr.getField();
                                aux = hcorr.getCurrent()+correctionFactor * hcorr.toCurrentFromField(kickH[i]);
                                if (Math.abs(aux) > block.getMaxCorr()){
                                    if (aux > 0){
                                        aux = block.getMaxCorr();
                                    } else {
                                        aux = -1*block.getMaxCorr();
                                    }
                                }

                            } catch (GetException ex) {
                                Logger.getLogger(FXMLController.class.getName()).log(Level.SEVERE, null, ex);
                            }
                            //aux = aux  + correctionFactor * hcorr.toCurrentFromField(kickH[i]);
                            i++;
                            try {
                                //hcorr.setField(aux);                                
                                hcorr.setCurrent(aux);
                            } catch (PutException ex) {
                                Logger.getLogger(FXMLController.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }
                    }
                }
                j = 0;
                if (radioButtonVertical.isSelected() || radioButtonHorVer.isSelected()) {
                    for (DipoleCorr vcorr : matrix.VC) {
                        if (!abortFlag) {
                            try {
                                //aux = vcorr.getField();
                                aux = vcorr.getCurrent()+ correctionFactor * vcorr.toCurrentFromField(kickV[j]);
                                if (Math.abs(aux) > block.getMaxCorr()){
                                    if (aux > 0){
                                        aux = block.getMaxCorr();
                                    } else {
                                        aux = -1*block.getMaxCorr();
                                    }
                                }
                            } catch (GetException ex) {
                                Logger.getLogger(FXMLController.class.getName()).log(Level.SEVERE, null, ex);
                            }
                            //aux = aux + correctionFactor * vcorr.toCurrentFromField(kickV[j]);
                            j++;
                            try {
                                //vcorr.setField(aux);
                                vcorr.setCurrent(aux);
                                //System.out.print(vcorr.getMainSupply().getChannel("I_Set").channelName());
                            } catch (PutException ex) {
                                Logger.getLogger(FXMLController.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }
                    }
                }
            }
            progressBarCorrection.setProgress((MainFunctions.mainDocument.CorrectionElementsSelected.indexOf(block) + 1) / MainFunctions.mainDocument.CorrectionElementsSelected.size());
        }
        labelProgressCorrection.setVisible(false);
        progressBarCorrection.setVisible(false);
        progressBarCorrection.setProgress(0);

    }

    @FXML
    private void handleContextMenu(ContextMenuEvent event) {

        final MenuItem addBlock = new MenuItem("Add Block");
        final MenuItem loadBlock = new MenuItem("Load Block");
        final MenuItem editBlock = new MenuItem("Edit Selected Block");
        final MenuItem renameBlock = new MenuItem("Rename Selected Block");
        final MenuItem deleteBlock = new MenuItem("Remove Selected Block");
        final MenuItem saveBlock = new MenuItem("Save Selected Block");
        final ContextMenu menu;
        final CorrectionBlock blockItem = listViewBlockDefinition.getSelectionModel().getSelectedItem();

        if (blockItem == null) {
            menu = new ContextMenu(addBlock, loadBlock);
        } else {
            menu = new ContextMenu(addBlock, loadBlock, new SeparatorMenuItem(), renameBlock, editBlock, deleteBlock, saveBlock);
        }

        addBlock.setOnAction((ActionEvent event1) -> {
            Stage stage;
            Parent root;
            URL url = null;
            String sceneFile = "/fxml/CorrectionElementSelection.fxml";
            try {
                stage = new Stage();
                url = getClass().getResource(sceneFile);
                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(MainApp.class.getResource(sceneFile));
                root = loader.load();
                root.getStylesheets().add("/styles/Styles.css");
                stage.setScene(new Scene(root));
                stage.setTitle("Elements for Correction: Block Definition");
                stage.initModality(Modality.APPLICATION_MODAL);
                stage.initOwner(listViewBlockDefinition.getScene().getWindow());
                CorrectionElementSelectionController loginController = loader.getController();
                loginController.loggedInProperty().addListener((obs, wasLoggedIn, isNowLoggedIn) -> {
                    if (isNowLoggedIn) {
                        if (loginController.getChangedSelectionList()) {
                            CorrectionBlock newBlock = new CorrectionBlock();
                            newBlock.setBlockBPM(loginController.getBPMSelectionList());
                            newBlock.setBlockHC(loginController.getHCSelectionList());
                            newBlock.setBlockVC(loginController.getVCSelectionList());
                            newBlock.setblockName(loginController.getBlockName());
                            newBlock.setMaxCor(loginController.getMaxCur());
                            newBlock.setOk1to1(false);
                            newBlock.setOkSVD(false);                                                                                                           
                            
                            MainFunctions.mainDocument.CorrectionElements.forEach(block -> {
                                if (block.getblockName().equals(newBlock.getblockName())) {
                                    newBlock.setblockName(newBlock.getblockName() + "_new");
                                }

                            });
                            MainFunctions.mainDocument.CorrectionElementsSelected.forEach(block -> {
                                if (block.getblockName().equals(newBlock.getblockName())) {
                                    newBlock.setblockName(newBlock.getblockName() + "_new");
                                }

                            });
                            newBlock.initialize1to1Correction(MainFunctions.mainDocument.getAccelerator());
                            newBlock.initializeSVDCorrection(MainFunctions.mainDocument.getAccelerator());
                            MainFunctions.mainDocument.CorrectionElements.add(newBlock);
                            MainFunctions.mainDocument.CorrectionElements.sort((ele1, ele2) -> Double.compare(ele1.getBlockBPM().get(0).getSDisplay(), ele2.getBlockBPM().get(0).getSDisplay()));
                        }
                        stage.close();
                    }
                });
                loginController.populateElementGrid(MainFunctions.mainDocument.getAccelerator());
                stage.showAndWait();                    
            } catch (IOException ex) {
                System.out.println("Exception on FXMLLoader.load()");
                System.out.println("  * url: " + url);
                System.out.println("  * " + ex);
                System.out.println("    ----------------------------------------\n");
                try {
                    throw ex;
                } catch (IOException ex1) {
                    Logger.getLogger(FXMLController.class.getName()).log(Level.SEVERE, null, ex1);
                }
            }            
        });

        editBlock.setOnAction((ActionEvent event1) -> {
            Stage stage;
            Parent root;
            URL url = null;
            String sceneFile = "/fxml/CorrectionElementSelection.fxml";
            try {
                stage = new Stage();
                url = getClass().getResource(sceneFile);
                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(MainApp.class.getResource(sceneFile));
                root = loader.load();
                root.getStylesheets().add("/styles/Styles.css");
                stage.setScene(new Scene(root));
                stage.setTitle("Elements for Correction: Block Definition");
                stage.initModality(Modality.APPLICATION_MODAL);
                stage.initOwner(listViewBlockDefinition.getScene().getWindow());
                CorrectionElementSelectionController loginController = loader.getController();
                loginController.populateElementGrid(MainFunctions.mainDocument.getAccelerator());
                loginController.enterElementstoEdit(MainFunctions.mainDocument.CorrectionElements.get(listViewBlockDefinition.getSelectionModel().getSelectedIndex()));
                loginController.setBlockName(MainFunctions.mainDocument.CorrectionElements.get(listViewBlockDefinition.getSelectionModel().getSelectedIndex()).getblockName());
                loginController.setMaxCur(MainFunctions.mainDocument.CorrectionElements.get(listViewBlockDefinition.getSelectionModel().getSelectedIndex()).getMaxCorr());
                loginController.loggedInProperty().addListener((obs, wasLoggedIn, isNowLoggedIn) -> {
                    if (isNowLoggedIn) {
                        if (loginController.getChangedSelectionList()) {
                            MainFunctions.mainDocument.CorrectionElements.get(listViewBlockDefinition.getSelectionModel().getSelectedIndex()).setBlockBPM(loginController.getBPMSelectionList());
                            MainFunctions.mainDocument.CorrectionElements.get(listViewBlockDefinition.getSelectionModel().getSelectedIndex()).setBlockHC(loginController.getHCSelectionList());
                            MainFunctions.mainDocument.CorrectionElements.get(listViewBlockDefinition.getSelectionModel().getSelectedIndex()).setBlockVC(loginController.getVCSelectionList());
                            MainFunctions.mainDocument.CorrectionElements.get(listViewBlockDefinition.getSelectionModel().getSelectedIndex()).setOk1to1(false);
                            MainFunctions.mainDocument.CorrectionElements.get(listViewBlockDefinition.getSelectionModel().getSelectedIndex()).setOkSVD(false);
                            MainFunctions.mainDocument.CorrectionElements.get(listViewBlockDefinition.getSelectionModel().getSelectedIndex()).initialize1to1Correction(MainFunctions.mainDocument.getAccelerator());
                            MainFunctions.mainDocument.CorrectionElements.get(listViewBlockDefinition.getSelectionModel().getSelectedIndex()).initializeSVDCorrection(MainFunctions.mainDocument.getAccelerator());
                            MainFunctions.mainDocument.CorrectionElements.sort((ele1, ele2) -> Double.compare(ele1.getBlockBPM().get(0).getSDisplay(), ele2.getBlockBPM().get(0).getSDisplay()));  
                            MainFunctions.mainDocument.CorrectionElements.get(listViewBlockDefinition.getSelectionModel().getSelectedIndex()).setMaxCor(loginController.getMaxCur());
                        }
                        stage.close();
                    }
                });
                stage.showAndWait();
            } catch (IOException ex) {
                System.out.println("Exception on FXMLLoader.load()");
                System.out.println("  * url: " + url);
                System.out.println("  * " + ex);
                System.out.println("    ----------------------------------------\n");
                try {
                    throw ex;
                } catch (IOException ex1) {
                    Logger.getLogger(FXMLController.class.getName()).log(Level.SEVERE, null, ex1);
                }
            }
        });
        
        renameBlock.setOnAction((ActionEvent event1) -> {
                
            CorrectionBlock selectedBlock = listViewBlockDefinition.getSelectionModel().getSelectedItem();

            if (selectedBlock != null) {
                //Dialog for Block name
                TextInputDialog dialog = new TextInputDialog(selectedBlock.getblockName());
                dialog.setTitle("Block Name"); 
                dialog.setContentText("Please enter the block name:");

                // Traditional way to get the response value.
                Optional<String> result = dialog.showAndWait();    

                // If there is a result present sets it to the block name
                result.ifPresent(name -> selectedBlock.setblockName(name));
            }
                
        });

        deleteBlock.setOnAction((ActionEvent event1) -> {
            int index = listViewBlockDefinition.getSelectionModel().getSelectedIndex();
            CorrectionBlock removeName = listViewBlockDefinition.getSelectionModel().getSelectedItem();
            if (removeName != null) {
                MainFunctions.mainDocument.CorrectionElements.remove(removeName);
                listViewBlockDefinition.getSelectionModel().clearSelection();
            }
        });

        loadBlock.setOnAction((ActionEvent event1) -> {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Load new correction block");

            //Set extension filter
            FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("XML files (*.xml)", "*.xml");
            fileChooser.getExtensionFilters().add(extFilter);

            //Show save file dialog
            File selectedFile = fileChooser.showOpenDialog(null);
            if (selectedFile != null) {
                CorrectionBlock newBlock = new CorrectionBlock();
                newBlock.loadBlock(selectedFile.getAbsoluteFile(), MainFunctions.mainDocument.getAccelerator());
                MainFunctions.mainDocument.CorrectionElements.forEach(block -> {
                    if (block.getblockName().equals(newBlock.getblockName())) {
                        newBlock.setblockName(newBlock.getblockName() + "_new");
                    }

                });
                MainFunctions.mainDocument.CorrectionElementsSelected.forEach(block -> {
                    if (block.getblockName().equals(newBlock.getblockName())) {
                        newBlock.setblockName(newBlock.getblockName() + "_new");
                    }

                });
                MainFunctions.mainDocument.CorrectionElements.add(newBlock);
                MainFunctions.mainDocument.CorrectionElements.sort((ele1, ele2) -> Double.compare(ele1.getBlockBPM().get(0).getSDisplay(), ele2.getBlockBPM().get(0).getSDisplay()));
            }
        });

        saveBlock.setOnAction((ActionEvent event1) -> {
            CorrectionBlock newBlock = new CorrectionBlock();
            newBlock = listViewBlockDefinition.getSelectionModel().getSelectedItem();

            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Save correction block");
            fileChooser.setInitialFileName(newBlock.getblockName());

            //Set extension filter
            FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("XML files (*.xml)", "*.xml");
            fileChooser.getExtensionFilters().add(extFilter);

            //Show save file dialog
            File selectedFile = fileChooser.showSaveDialog(null);
            if (selectedFile != null) {
                XmlDataAdaptor da = XmlDataAdaptor.newEmptyDocumentAdaptor();
                DataAdaptor trajectoryAdaptor = da.createChild("CorrectionData");
                trajectoryAdaptor.setValue("title", selectedFile.getAbsolutePath());
                trajectoryAdaptor.setValue("date", new SimpleDateFormat("yyyy-MM-dd HH:mm").format(new Date()));
                newBlock.saveBlock(trajectoryAdaptor);
                try {
                    da.writeTo(selectedFile.getAbsoluteFile());
                } catch (IOException ex) {
                    Logger.getLogger(FXMLController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

        menu.show(listViewBlockDefinition.getScene().getWindow(), event.getScreenX(), event.getScreenY());

    }

    @FXML
    private void handleRemoveSelectedBlock(ActionEvent event) {
        CorrectionBlock removeName = listViewBlockSelection.getSelectionModel().getSelectedItem();
        if (removeName != null) {
            MainFunctions.mainDocument.CorrectionElements.add(removeName);
            MainFunctions.mainDocument.CorrectionElements.sort((ele1, ele2) -> Double.compare(ele1.getBlockBPM().get(0).getSDisplay(), ele2.getBlockBPM().get(0).getSDisplay()));
            MainFunctions.mainDocument.CorrectionElementsSelected.remove(removeName);
            listViewBlockSelection.getSelectionModel().clearSelection();
        }
    }

    @FXML
    private void handleSelectedBlock(ActionEvent event) {
        CorrectionBlock includeName = listViewBlockDefinition.getSelectionModel().getSelectedItem();
        if (includeName != null) {
            MainFunctions.mainDocument.CorrectionElementsSelected.add(includeName);
            MainFunctions.mainDocument.CorrectionElementsSelected.sort((ele1, ele2) -> Double.compare(ele1.getBlockBPM().get(0).getSDisplay(), ele2.getBlockBPM().get(0).getSDisplay()));
            MainFunctions.mainDocument.CorrectionElements.remove(includeName);
            listViewBlockDefinition.getSelectionModel().clearSelection();
        }

    }

    @FXML
    private void handleChooseRefTrajectory(ActionEvent event) {
        //read new reference trajectory file
        try {
            MainFunctions.mainDocument.DisplayTraj.readReferenceTrajectory(MainFunctions.mainDocument.getAccelerator(), comboBoxRefTrajectory.getSelectionModel().getSelectedItem());
        } catch (IOException ex) {
            Logger.getLogger(FXMLController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void handleCorrectTrajectory(ActionEvent event) throws PutException, MonitorException, InterruptedException, GetException, ConnectionException {
        boolean correctSVDFlag = true;
        boolean correct1to1Flag = true;

        //enable Abort button
        //buttonAbort.setDisable(false);
        //check trajectory
        //final List<BPM> BPMList = new ArrayList<>();
        //final int maxBPM = 0;
        //BPMList.addAll(MainFunctions.mainDocument.getAccelerator().getAllNodesOfType("BPM")); 
        
        //Updates Reference Trajecotry to file
        try {
            MainFunctions.mainDocument.DisplayTraj.readReferenceTrajectory(MainFunctions.mainDocument.getAccelerator(), comboBoxRefTrajectory.getValue());
        } catch (IOException ex) {
            Logger.getLogger(FXMLController.class.getName()).log(Level.SEVERE, null, ex);
        }

        for (CorrectionBlock blockName : MainFunctions.mainDocument.CorrectionElementsSelected) {
             MainFunctions.mainDocument.DisplayTraj.readTrajectory(blockName.getBlockBPM());
            if (!blockName.isOkSVD() || Math.abs(MainFunctions.mainDocument.DisplayTraj.getXmax()) > MainFunctions.mainDocument.getTrajExpertLimit() || Math.abs(MainFunctions.mainDocument.DisplayTraj.getYmax()) >  MainFunctions.mainDocument.getTrajExpertLimit()) {
                //System.out.print(blockName.getblockName() + ":" + !blockName.isOkSVD() + " " + Math.abs(MainFunctions.mainDocument.DisplayTraj.getXmax()) + " " + Math.abs(MainFunctions.mainDocument.DisplayTraj.getYmax()));
                correctSVDFlag = false;
            }
            if (!blockName.isOk1to1()) {
                correct1to1Flag = false;
            }
        }

        if (radiobuttonCorrectSVD.isSelected() && correctSVDFlag) {
            correctSVD();
        } else if (radiobuttonCorrect1to1.isSelected() && correct1to1Flag) {
            correct1to1();
        } else {
            Alert alert = new Alert(AlertType.WARNING);
            alert.setTitle("Warning Dialog");
            alert.setHeaderText(null);
            alert.setGraphic(new ImageView(this.getClass().getResource("/pictures/warning.png").toString()));
            alert.setContentText("One (or more) reponse matrix is not ready for correction or the trajectory limit is exceeded! Please check the block status and trajectory before correction.");
            alert.show();
        }

        //disable Abort button
        //buttonAbort.setDisable(true);
        abortFlag = false;

    }

    @FXML
    private void handleMeasureResponseSVDfromModel(ActionEvent event) {
        CorrectionBlock blockName = listViewBlockSelection.getSelectionModel().getSelectedItem();

        if (blockName != null) {

            TextInputDialog dialog = new TextInputDialog("0.002");
            dialog.setTitle("Set Field Variation");
            dialog.setHeaderText(null);
            dialog.setContentText("Enter Corrector strength (T.m) step:");
            Optional<String> result = dialog.showAndWait();
            double resultval = 0.002;
            if (result.isPresent()) {
                resultval = Double.parseDouble(result.get());
                if (resultval <= 0.0 || resultval > 0.01) {
                    resultval = 0.002;
                }
            }

            final double Dk = resultval;

            //print knobs
            textAreaSVD.clear();

            //MainFunctions.mainDocument.CorrectionElementsSelected.get(blockName).initializeSVDCorrection(MainFunctions.mainDocument.getAccelerator());
            textAreaSVD.setText(textAreaSVD.getText() + blockName.getblockName() + "\n");
            textAreaSVD.setText(textAreaSVD.getText() + "Horizontal Correctors: \n");
            blockName.getBlockHC().forEach(hcorr -> textAreaSVD.setText(textAreaSVD.getText() + hcorr.toString() + "; "));
            textAreaSVD.setText(textAreaSVD.getText() + "\n");
            textAreaSVD.setText(textAreaSVD.getText() + "Vertical correctors: \n");
            blockName.getBlockVC().forEach(vcorr -> textAreaSVD.setText(textAreaSVD.getText() + vcorr.toString() + "; "));
            textAreaSVD.setText(textAreaSVD.getText() + "\n");

            Task<Void> task;
            task = new Task<Void>() {

                @Override
                protected Void call() throws Exception {

                    int progress = 0;                    
                    String synchronizationMode = null;
                    if (MainFunctions.mainDocument.getLiveModel()) {
                        synchronizationMode = "RF_DESIGN";
                    } else {
                        synchronizationMode = "DESIGN";
                    }
                    blockName.getCorrectionSVD().calculateTRMHorizontal(Dk, synchronizationMode);
                    updateProgress(1, 2);
                    blockName.getCorrectionSVD().calculateTRMVertical(Dk, synchronizationMode);
                    blockName.setOkSVD(true);
                    progress++;
                    updateProgress(2, 2);

                    //when the scan finishes set the label and progress bar to zero
                    labelProgressCorrection.setVisible(false);
                    progressBarCorrection.setVisible(false);
                    updateProgress(0, 2);
                    textAreaSVD.setText("RESPONSE CALCULATION: Finished!");
                    try {
                        MainFunctions.mainDocument.DisplayTraj.readTrajectory(MainFunctions.mainDocument.getAccelerator().getAllNodesOfType("BPM"));
                    } catch (ConnectionException | GetException ex) {
                        Logger.getLogger(FXMLController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    listViewBlockSelection.refresh();
                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            tabSVD.setGraphic(new ImageView(getClass().getResource("/pictures/ok.png").toString()));
                        }
                    });
                    buttonSVDfromModel.setDisable(false);
                    buttonVerifyMatrixSVD.setDisable(false);
                    buttonCalcCorrectSVD.setDisable(false);
                    if (Math.abs(MainFunctions.mainDocument.DisplayTraj.getXmax()) <  MainFunctions.mainDocument.getTrajExpertLimit() && Math.abs(MainFunctions.mainDocument.DisplayTraj.getYmax()) <  MainFunctions.mainDocument.getTrajExpertLimit()) {
                        buttonMeasureResponseSVD.setDisable(true);
                    } else {
                        buttonMeasureResponseSVD.setDisable(false);
                    }

                    return null;
                }
            ;

            };

            Thread calibrate = new Thread(task);
            calibrate.setDaemon(true); // thread will not prevent application shutdown
            progressBarCorrection.progressProperty().bind(task.progressProperty());
            if (result.isPresent()) {
                labelProgressCorrection.setVisible(true);
                progressBarCorrection.setVisible(true);
                labelProgressCorrection.setText("Calculating response matrix...");
                calibrate.start();
            }
        } else {
            Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("Information Dialog");
            alert.setHeaderText(null);
            alert.setContentText("Please select a correction block in the Selected column to measure.");
            alert.show();
        }

    }

    @FXML
    private void handleMeasureResponse1to1fromModel(ActionEvent event) {

        TextInputDialog dialog = new TextInputDialog("0.002");
        dialog.setTitle("Set Field Variation");
        dialog.setHeaderText(null);
        dialog.setContentText("Enter Corrector strength (T.m) step:");
        Optional<String> result = dialog.showAndWait();
        double resultval = 0.002;
        if (result.isPresent()) {
            resultval = Double.parseDouble(result.get());
            if (resultval <= 0.0 || resultval > 0.01) {
                resultval = 0.002;
            }
        }
        int blockIndex = listViewBlockSelection.getSelectionModel().getSelectedIndex();

        final double Dk = resultval;
        Task<Void> task;
        task = new Task<Void>() {

            @Override
            protected Void call() throws Exception {

                int progress = 0;
                int total = MainFunctions.mainDocument.CorrectionElementsSelected.get(blockIndex).getCorrection1to1().getHC().size() + MainFunctions.mainDocument.CorrectionElementsSelected.get(blockIndex).getCorrection1to1().getVC().size();
                String synchronizationMode = null;
                if (MainFunctions.mainDocument.getLiveModel()) {
                    synchronizationMode = "RF_DESIGN";
                } else {
                    synchronizationMode = "DESIGN";
                }
                //make horizontal calibration for each bpm
                for (BPM bpm : MainFunctions.mainDocument.CorrectionElementsSelected.get(blockIndex).getCorrection1to1().getHC().keySet()) {
                    MainFunctions.mainDocument.CorrectionElementsSelected.get(blockIndex).getCorrection1to1().simulHCalibration(bpm, Dk, synchronizationMode);
                    //update progressbar
                    progress++;
                    updateProgress(progress, total);
                }

                //make vertical calibration for each bpm
                for (BPM bpm : MainFunctions.mainDocument.CorrectionElementsSelected.get(blockIndex).getCorrection1to1().getVC().keySet()) {
                    MainFunctions.mainDocument.CorrectionElementsSelected.get(blockIndex).getCorrection1to1().simulVCalibration(bpm, Dk, synchronizationMode);
                    //update progressbar
                    progress++;
                    updateProgress(progress, total);
                }

                MainFunctions.mainDocument.CorrectionElementsSelected.get(blockIndex).setOk1to1(true);

                //when the scan finishes set the label and progress bar to zero
                labelProgressCorrection.setVisible(false);
                progressBarCorrection.setVisible(false);
                updateProgress(0, total);
                textArea1to1.setText("RESPONSE CALCULATION: Finished!");
                listViewBlockSelection.refresh();
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        tab1to1.setGraphic(new ImageView(getClass().getResource("/pictures/ok.png").toString()));
                    }
                });
                //buttonCheckPair.setDisable(false);
                //buttonMeasureResponse1to1.setDisable(false);
                //button1to1fromModel.setDisable(false);
                buttonVerifyResponse1to1.setDisable(false);

                return null;
            }
        ;

        };

        Thread calibrate = new Thread(task);
        calibrate.setDaemon(true); // thread will not prevent application shutdown
        if (result.isPresent()) {
            labelProgressCorrection.setVisible(true);
            progressBarCorrection.setVisible(true);
            labelProgressCorrection.setText("Aquiring BPM responses");
            progressBarCorrection.progressProperty().bind(task.progressProperty());
            calibrate.start();
        }

    }

    @FXML
    private void handleVerifySVDResponse(ActionEvent event) {
        Stage stage;
        Parent root;
        URL url = null;
        String sceneFile = "/fxml/PlotVerifyResponse.fxml";
        CorrectionBlock blockName = listViewBlockSelection.getSelectionModel().getSelectedItem();

        try {
            stage = new Stage();
            url = getClass().getResource(sceneFile);
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource(sceneFile));
            root = loader.load();
            root.getStylesheets().add("/styles/Styles.css");
            stage.setScene(new Scene(root));
            stage.setTitle("Verify SVD reponse for each corrector");
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.initOwner(comboBoxRefTrajectory.getScene().getWindow());
            VerifyResponseController loginController = loader.getController();
            blockName.getCorrectionSVD().setCutSVD(Double.parseDouble(textFieldSingularValCut.getText()));
            loginController.setCorrectionSVD(blockName.getCorrectionSVD());
            loginController.loggedInProperty().addListener((obs, wasLoggedIn, isNowLoggedIn) -> {
                if (isNowLoggedIn) {
                    stage.close();
                }
            });
            stage.showAndWait();
        } catch (IOException ex) {
            System.out.println("Exception on FXMLLoader.load()");
            System.out.println("  * url: " + url);
            System.out.println("  * " + ex);
            System.out.println("    ----------------------------------------\n");
            //throw ex;
        }
    }

    @FXML
    private void handleVerify1to1Response(ActionEvent event) {
        Stage stage;
        Parent root;
        URL url = null;
        String sceneFile = "/fxml/PlotVerifyResponse.fxml";
        CorrectionBlock blockName = listViewBlockSelection.getSelectionModel().getSelectedItem();

        try {
            stage = new Stage();
            url = getClass().getResource(sceneFile);
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource(sceneFile));
            root = loader.load();
            root.getStylesheets().add("/styles/Styles.css");
            stage.setScene(new Scene(root));
            stage.setTitle("Verify BPM response");
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.initOwner(comboBoxRefTrajectory.getScene().getWindow());
            VerifyResponseController loginController = loader.getController();
            loginController.setCorrection1to1(blockName.getCorrection1to1());
            loginController.loggedInProperty().addListener((obs, wasLoggedIn, isNowLoggedIn) -> {
                if (isNowLoggedIn) {
                    stage.close();
                }
            });
            stage.showAndWait();
        } catch (IOException ex) {
            System.out.println("Exception on FXMLLoader.load()");
            System.out.println("  * url: " + url);
            System.out.println("  * " + ex);
            System.out.println("    ----------------------------------------\n");
            //throw ex;
        }
    }

    @FXML
    private void handleSetSVDCutValue(ActionEvent event) {
        CorrectionBlock selectedName = listViewBlockSelection.getSelectionModel().getSelectedItem();
        if (selectedName != null) {
            selectedName.getCorrectionSVD().setCutSVD(Double.parseDouble(textFieldSingularValCut.getText()));
        }
    }

    @FXML
    private void handleAbortRequest(ActionEvent event) {
        textArea1to1.setText("Correction aborted by the user!");
        textAreaSVD.setText("Correction aborted by the user!");
        abortFlag = true;
    }

    @FXML
    private void handleTrajectoryShow(ActionEvent event) {
    }
}
