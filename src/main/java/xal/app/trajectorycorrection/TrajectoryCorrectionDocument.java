/*
 * Copyright (C) 2018 European Spallation Source ERIC
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package xal.app.trajectorycorrection;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Alert;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import xal.ca.ConnectionException;
import xal.ca.GetException;
import xal.extension.fxapplication.XalFxDocument;
import xal.tools.data.DataAdaptor;
import xal.tools.xml.XmlDataAdaptor;

/**
 *
 * @author Natalia Milas
 */
public class TrajectoryCorrectionDocument extends XalFxDocument {
    
    public BooleanProperty liveModel;

    public SimpleStringProperty refTrajectoryFile;

    public SimpleStringProperty displayTrajectoryFile;

    public SimpleStringProperty saveTrajectoryFile;

    public TrajectoryArray Trajectory;
    
    public Double trajExpertLimit;    
    
    public Double steerersExpertLimit;
    
    public ObservableList<CorrectionBlock> CorrectionElements = FXCollections.observableArrayList();  //List of defined Blocks
    
    public ObservableList<CorrectionBlock> CorrectionElementsSelected = FXCollections.observableArrayList();  //List selected blocks (can be used in correction)
    
    public final ObservableList<URL> refTrajData = FXCollections.observableArrayList();// holds info about reference trajectories
    
    public TrajectoryArray DisplayTraj = new TrajectoryArray();//Trajectory to be displayed on the plot

    /* CONSTRUCTOR
     * in order to access to variable from the main class
     */
    public TrajectoryCorrectionDocument(Stage stage) {
        super(stage);
        DEFAULT_FILENAME = "Trajectory.xml";
        WILDCARD_FILE_EXTENSION = "*.xml";
        HELP_PAGEID = "227688944";
        
        liveModel = new SimpleBooleanProperty();
        refTrajectoryFile = new SimpleStringProperty();
        displayTrajectoryFile = new SimpleStringProperty();
        saveTrajectoryFile = new SimpleStringProperty();
        
        trajExpertLimit = 5000.0;
        steerersExpertLimit = 0.2;

        liveModel.setValue(false);
        refTrajectoryFile.setValue(null);
        displayTrajectoryFile.setValue(null);
        saveTrajectoryFile.setValue(null);
    }
    
    public void saveTrajectory() {

        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Save Trajectory File");

        //Set extension filter
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("XML files (*.xml)", "*.xml");
        fileChooser.getExtensionFilters().add(extFilter);

        //Show save file dialog
        File selectedFile = fileChooser.showSaveDialog(null);

        saveTrajectoryFile.set(selectedFile.toString());

    }

    public void loadRefTrajectory() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Load Reference Trajectory File");

        //Set extension filter
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("XML files (*.xml)", "*.xml");
        fileChooser.getExtensionFilters().add(extFilter);

        //Show save file dialog
        File selectedFile = fileChooser.showOpenDialog(null);

        refTrajectoryFile.set(selectedFile.toString());

    }    

    @Override
    public void saveDocumentAs(URL url) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Save Application State");
        fileChooser.setInitialFileName("TrajectoryCorrectionAppState");

        //Set extension filter
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("XML files (*.xml)", "*.xml");
        fileChooser.getExtensionFilters().add(extFilter);

        //Show save file dialog
        File selectedFile = fileChooser.showSaveDialog(null);
        if (selectedFile != null) {
            XmlDataAdaptor da = XmlDataAdaptor.newEmptyDocumentAdaptor();
            DataAdaptor appstateAdaptor = da.createChild("ApplicationState");
            appstateAdaptor.setValue("title", selectedFile.getAbsolutePath());
            appstateAdaptor.setValue("date", new SimpleDateFormat("yyyy-MM-dd HH:mm").format(new Date()));
            DataAdaptor generalAdaptor = appstateAdaptor.createChild("GeneralData");
            generalAdaptor.setValue("Trajectory_Limit", trajExpertLimit);
            generalAdaptor.setValue("Steerer_Limit", steerersExpertLimit);
            DataAdaptor refTrajectoryAdaptor = appstateAdaptor.createChild("ReferenceTrajectoryData");
            refTrajData.forEach(file -> {
                if (!new File(file.toString()).getName().contains("Zero")) {
                    try {
                        DisplayTraj.saveTrajectory(this.getAccelerator(), file, refTrajectoryAdaptor);
                    } catch (ConnectionException | GetException ex) {
                        Logger.getLogger(FXMLController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            });
            DataAdaptor blockAdaptor = appstateAdaptor.createChild("CorrectionBlockData");
            CorrectionElements.forEach(block -> block.saveBlock(blockAdaptor));
            CorrectionElementsSelected.forEach(block -> block.saveBlock(blockAdaptor));
            try {
                da.writeTo(selectedFile.getAbsoluteFile());
            } catch (IOException ex) {
                Logger.getLogger(FXMLController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public void loadDocument(URL url) {
        DataAdaptor readAdp = null;
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Load Application State");

        //Set extension filter
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("XML files (*.xml)", "*.xml");
        fileChooser.getExtensionFilters().add(extFilter);
        CorrectionElements.clear();
        CorrectionElementsSelected.clear();
        refTrajData.forEach(file -> {
            if (!new File(file.toString()).getName().contains("Zero")) {
                refTrajData.remove(file);
            }
        });

        //Show save file dialog
        File selectedFile = fileChooser.showOpenDialog(null);
        if (selectedFile != null) {
            try {
                readAdp = XmlDataAdaptor.adaptorForFile(selectedFile, false);
                DataAdaptor headerAdaptor = readAdp.childAdaptor("ApplicationState");
                DataAdaptor generalAdaptor = headerAdaptor.childAdaptor("GeneralData");
                setTrajExpertLimit(generalAdaptor.doubleValue("Trajectory_Limit"));
                setSteerersExpertLimit(generalAdaptor.doubleValue("Steerer_Limit"));
                XmlDataAdaptor trajectoryAdaptor = (XmlDataAdaptor) headerAdaptor.childAdaptor("ReferenceTrajectoryData");
                trajectoryAdaptor.childAdaptors().forEach(trajFileAdaptor -> {
                    try {
                        refTrajData.add(new URL(trajFileAdaptor.stringValue("title")));
                    } catch (MalformedURLException ex) {
                        File file = new File(trajFileAdaptor.stringValue("title"));
                        if (file.exists()) {
                            try {
                                refTrajData.add(file.toURI().toURL());
                            } catch (MalformedURLException ex1) {
                                Logger.getLogger(FXMLController.class.getName()).log(Level.SEVERE, null, ex1);
                            }
                        } else {
                            Alert alert = new Alert(Alert.AlertType.INFORMATION);
                            alert.setTitle("Error Dialog");
                            alert.setHeaderText(null);
                            alert.setContentText("Reference trajectory file doesn't exist.");
                        }
                    }
                });
                XmlDataAdaptor blockAdaptor = (XmlDataAdaptor) headerAdaptor.childAdaptor("CorrectionBlockData");
                blockAdaptor.childAdaptors().forEach(chilblockAdaptor -> {
                    CorrectionBlock newBlock = new CorrectionBlock();
                    newBlock.loadBlock(chilblockAdaptor, this.getAccelerator());
                    CorrectionElements.add(newBlock);
                });
                CorrectionElements.sort((ele1, ele2) -> Double.compare(ele1.getBlockBPM().get(0).getSDisplay(), ele2.getBlockBPM().get(0).getSDisplay()));
            } catch (MalformedURLException | XmlDataAdaptor.ParseException | XmlDataAdaptor.ResourceNotFoundException ex) {
                Logger.getLogger(FXMLController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

   public Boolean getLiveModel() {
        return liveModel.get();
    }

    public void setLiveModel(boolean liveModel) {
        this.liveModel.set(liveModel);
    }    

    public Double getTrajExpertLimit() {
        return trajExpertLimit;
    }

    public void setTrajExpertLimit(double trajExpertLimit) {
        this.trajExpertLimit = trajExpertLimit;
    }

    public Double getSteerersExpertLimit() {
        return steerersExpertLimit;
    }

    public void setSteerersExpertLimit(double steerersExpertLimit) {
        this.steerersExpertLimit = steerersExpertLimit;
    }        
    
    @Override
    public void newDocument() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
