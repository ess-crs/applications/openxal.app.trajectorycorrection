/*
 * Copyright (C) 2018 European Spallation Source ERIC
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package xal.app.trajectorycorrection;

import java.io.IOException;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import static javafx.application.Application.launch;
import javafx.application.Platform;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckMenuItem;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.RadioMenuItem;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.util.Pair;

import xal.extension.fxapplication.FxApplication;
import xal.rbac.AccessDeniedException;
import xal.rbac.Credentials;
import xal.rbac.RBACException;
import xal.rbac.RBACLogin;
import xal.rbac.RBACSubject;

public class MainApp extends FxApplication {

    @Override
    public void setup(Stage stage) {
        MAIN_SCENE = "/fxml/MainPanel.fxml";
        CSS_STYLE = "/styles/Styles.css";
        setApplicationName("Trajectory Correction");
        DOCUMENT = new TrajectoryCorrectionDocument(stage);
        HAS_SEQUENCE = false;

        MainFunctions.initialize((TrajectoryCorrectionDocument) DOCUMENT);
        
        //super.initialize();                                     
        //super.start(stage);

    }

    /**
     * The main() method is ignored in correctly deployed JavaFX application.
     * main() serves only as fallback in case the application can not be
     * launched through deployment artifacts, e.g., in IDEs with limited FX
     * support. NetBeans ignores main().
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

    
    @Override
    public void beforeStart(Stage stage) {

        Menu modelMenu = new Menu("Model");        
        RadioMenuItem modelLiveMenu = new RadioMenuItem("LIVE");
        modelLiveMenu.setOnAction(new LiveModelMenu((TrajectoryCorrectionDocument) DOCUMENT));
        modelMenu.getItems().add(modelLiveMenu);
        RadioMenuItem modelDesignMenu = new RadioMenuItem("DESIGN");
        modelDesignMenu.setOnAction(new DesignModelMenu((TrajectoryCorrectionDocument) DOCUMENT));
        ToggleGroup Modelgroup = new ToggleGroup();
        modelLiveMenu.setToggleGroup(Modelgroup);
        modelDesignMenu.setToggleGroup(Modelgroup);
        modelDesignMenu.setSelected(true);
        modelMenu.getItems().add(modelDesignMenu);
        
        Menu trajectoryMenu = new Menu("Reference Trajectory");        
        MenuItem loadReferenceTrajMenu = new MenuItem("Load trajectory");
        loadReferenceTrajMenu.setOnAction(new LoadReferenceTrajectoryMenu((TrajectoryCorrectionDocument) DOCUMENT));
        trajectoryMenu.getItems().add(loadReferenceTrajMenu);
        MenuItem saveTrajMenu = new MenuItem("Save trajectory");
        saveTrajMenu.setOnAction(new SaveTrajectoryMenu((TrajectoryCorrectionDocument) DOCUMENT));
        trajectoryMenu.getItems().add(saveTrajMenu);
        
        Menu trajectoryDispMenu = new Menu("Trajectory Display");        
        MenuItem openTrajDispMenu = new MenuItem("Open Trajecotry Display App");
        trajectoryDispMenu.setOnAction(new OpenTrajDisplayApp());
        
        Menu expertMenu = new Menu("Expert Pannel");        
        MenuItem expertTrajLimMenu = new MenuItem("Set Correction Limits");
        expertTrajLimMenu.setOnAction(new ExpertLimitMenu((TrajectoryCorrectionDocument) DOCUMENT));
        expertMenu.getItems().add(expertTrajLimMenu);        
        
        MENU_BAR.getMenus().add(MENU_BAR.getMenus().size() - 2, modelMenu);
        MENU_BAR.getMenus().add(MENU_BAR.getMenus().size() - 2, trajectoryMenu);
        MENU_BAR.getMenus().add(MENU_BAR.getMenus().size() - 2, trajectoryDispMenu);
        MENU_BAR.getMenus().add(MENU_BAR.getMenus().size() - 2, expertMenu);
    }
    
}

class LiveModelMenu implements EventHandler {

    protected TrajectoryCorrectionDocument document;

    public LiveModelMenu(TrajectoryCorrectionDocument document) {
        this.document = document;
    }

    @Override
    public void handle(Event t) {
        document.setLiveModel(true);
   }
}

class DesignModelMenu implements EventHandler {

    protected TrajectoryCorrectionDocument document;

    public DesignModelMenu(TrajectoryCorrectionDocument document) {
        this.document = document;
    }

    @Override
    public void handle(Event t) {
        document.setLiveModel(false);
    }
}

class LoadReferenceTrajectoryMenu implements EventHandler {

    protected TrajectoryCorrectionDocument document;

    public LoadReferenceTrajectoryMenu(TrajectoryCorrectionDocument document) {
        this.document = document;
    }

    @Override
    public void handle(Event t) {
       document.loadRefTrajectory();
    }
}

class SaveTrajectoryMenu implements EventHandler {

    protected TrajectoryCorrectionDocument document;

    public SaveTrajectoryMenu(TrajectoryCorrectionDocument document) {
        this.document = document;
    }

    @Override
    public void handle(Event t) {
        document.saveTrajectory();
    }
}

class OpenTrajDisplayApp implements EventHandler {

    @Override
    public void handle(Event t) {    

    try {
           Runtime.getRuntime().exec("java -jar ./openxal.app.trajectorydisplay/target/openxal.app.trajectorydisplay-4.1-SNAPSHOT.jar ");
        } catch (IOException ex) {
            Logger.getLogger(FXMLController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}

class ExpertLimitMenu implements EventHandler {

    protected TrajectoryCorrectionDocument document;

    public ExpertLimitMenu(TrajectoryCorrectionDocument document) {
        this.document = document;
    }
    
    @Override
    public void handle(Event t) {    
        /* RBAC service */
        boolean userData = authenticateWithRBAC();

        if (userData) {
            // Create the custom dialog.
            Dialog<Pair<String, String>> dialog = new Dialog<>();
            dialog.setTitle("expert Limit Dialog");
            dialog.setHeaderText("WARNING! This parameter is for expert use only.");

            // Set the icon (must be included in the project).
            dialog.setGraphic(new ImageView(this.getClass().getResource("/pictures/warning.png").toString()));

            // Set the button types.
            ButtonType applyButtonType = new ButtonType("Apply", ButtonData.OK_DONE);
            dialog.getDialogPane().getButtonTypes().addAll(applyButtonType, ButtonType.CANCEL);

            // Create the username and password labels and fields.
            GridPane grid = new GridPane();
            grid.setHgap(10);
            grid.setVgap(10);
            grid.setPadding(new Insets(20, 150, 10, 10));

            TextField svdField = new TextField(String.format("%.4f", document.getTrajExpertLimit()));
            svdField.setPromptText("Max. trajectory displacement for SVD (mm)");
            TextField steererField = new TextField(String.format("%.4f", document.getSteerersExpertLimit()));
            steererField.setPromptText("Max. Steerer Strength (T):");

            grid.add(new Label("Max. trajectory displacement for SVD (mm)"), 0, 0);
            grid.add(svdField, 1, 0);
            grid.add(new Label("Max. Steerer Strength (T):"), 0, 1);
            grid.add(steererField, 1, 1);

            // Enable/Disable Apply button depending on whether a any of the fields is empty
            Node applyButton = dialog.getDialogPane().lookupButton(applyButtonType);
            applyButton.setDisable(true);

            // Do some validation (using the Java 8 lambda syntax)
            svdField.textProperty().addListener((observable, oldValue, newValue) -> {
                applyButton.setDisable(newValue.trim().isEmpty());
            });
            steererField.textProperty().addListener((observable, oldValue, newValue) -> {
                applyButton.setDisable(newValue.trim().isEmpty());
            });

            dialog.getDialogPane().setContent(grid);

            // Request focus on the username field by default.
            Platform.runLater(() -> svdField.requestFocus());

            // Convert the result to a username-password-pair when the login button is clicked.
            dialog.setResultConverter(dialogButton -> {
                if (dialogButton == applyButtonType) {
                    return new Pair<>(svdField.getText(), steererField.getText());
                }
                return null;
            });

            Optional<Pair<String, String>> result = dialog.showAndWait();

            result.ifPresent(pair -> {
                document.setTrajExpertLimit(Double.parseDouble(pair.getKey()));
                document.setSteerersExpertLimit(Double.parseDouble(pair.getValue()));                
            });

            
            
        }
    }
    
     /**
     * Convention method for authenticating user.
     *
     * <p>
     * If RBACPlugin couldn't be loaded the application will not use RBAC !!!
     *
     * @return true if authentication successful, false if not.
     */
    private boolean authenticateWithRBAC() {

        RBACLogin rbacLogin;
        RBACSubject rbacSubject;
        //RBAC authentication
        try {
            rbacLogin = RBACLogin.newRBACLogin();
        } catch (RuntimeException e) {
            System.err.println("RBAC plugin not found. Continuing without RBAC.");
            return true;
        }

        try {
            // Try to use the local token.
            rbacSubject = rbacLogin.authenticate(null, null);
            System.out.println("Already logged in.");
            if (rbacSubject != null) {
                return true;
            }
        } catch (AccessDeniedException | RBACException e) {
            // Fall to authentication pane
        }

        try {
            System.out.println("Starting authentication.");

            final Credentials credentials = AuthenticationPaneFX.getCredentials();
            if (credentials == null) {
                // User pressed cancel
                System.out.println("Exiting...");
                return false;
            }
            rbacSubject = rbacLogin.authenticate(credentials.getUsername(), credentials.getPassword(), credentials.getPreferredRole(), credentials.getIP());
            System.out.printf("Authentication successful with username %s.\n", credentials.getUsername());
            return (rbacSubject != null);
        } catch (AccessDeniedException e) {
            System.err.printf("Access denied during authentication: %s\n", e.getMessage());
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Access denied");
            alert.setHeaderText("Access denied during authentication");
            alert.setContentText(e.getMessage());
            alert.showAndWait();
            return false;
        } catch (RBACException e) {
            System.err.printf("Error while trying to authenticate: %s\n", e.getMessage());
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Access denied");
            alert.setHeaderText("Error while trying to authenticate");
            alert.setContentText(e.getMessage());
            alert.showAndWait();
            return false;
        }
    }
    
}

